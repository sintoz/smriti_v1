<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');



class My_model extends CI_Model {

    var $data = '';

    function __construct() {
        parent::__construct();
        $this->load->library('ion_auth');
        
//        error_reporting(0);

        $this->category_array = array();
        
        define("project_name", "Smriti Design");
    }
    
    function settings(){
       $settings_row= $this->RowData("ms_settings", 1, "id");
       return $settings_row;
    }
    
    function product_types() {
        $product_types = array("1" => array(
                "name" => "product",
                "nav_icon" => "icon-briefcase"
            )
        );
        return $product_types;
    }
    
    function product_category_types() {
        $product_category_types = array("2" => array(
                "name" => "category",
                "nav_icon" => "icon-briefcase"
            ),
            "3" => array(
                "name" => "brand",
                "nav_icon" => "icon-home"
        
            )
        );
        return $product_category_types;
    }
    
    function media_types() {
        $media_types = array("4" => array(
                "name" => "design",
                "nav_icon" => "fa fa-file-photo-o",
                "home_icon" => "hi-icon-pencil"
            ),
            "5" => array(
                "name" => "animation",
                "nav_icon" => "fa fa-file-movie-o",
                "home_icon" => "hi-icon-videos"
            ),
            "6" => array(
                "name" => "web",
                "nav_icon" => "fa fa-globe",
                "home_icon" => "hi-icon-earth"
            )
        );
        return $media_types;
    }
    

    function RowData($table, $eventid, $field) {
        ini_set('max_execution_time', 0);

        $this->db->where(array($field => $eventid));
        return $result = $this->db->get($table)->row();
    }
    
    function ResultData($table, $order_column, $order_type, $conditional_array,$extra_parameter_array) {
        ini_set('max_execution_time', 0);
        
        $this->db->where($conditional_array);
        if(!empty($extra_parameter_array)){
            if(array_key_exists("limit", $extra_parameter_array)){
                $this->db->limit($extra_parameter_array["limit"]);
            }
        }
        $this->db->order_by($order_column, $order_type);
        $query = $this->db->get($table);

        if ($query->num_rows() >= 1) {
            return $query->result();
        } else {
            return false;
        }
    }
    
    function DeleteData($table, $eventid, $field) {
        $this->db->where(array($field => $eventid));
        $this->db->delete($table);
    }  
    
    
    
    function upload_rules(){
        $upload=array();
        $upload['upload_max_size']="2";
        $upload['upload_mime'] = array(
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            "png" => "image/png",
            "pdf" => "application/pdf",
            "word" => "application/msword",
            "word" => "application/octet-stream",
            "doc" => "application/msword",
            "docx"=>'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            "gif" => "image/gif");
        return $upload;
    }
    
    function imag_extension_array(){
      return $upload = array('jpeg','jpg',"png");
    }
    
    
    
    function upload_data($fileName){
        $file_name=project_name; 
        $myfiles = $this->my_model->diverse_array($_FILES[$fileName]);
        $upload=$this->my_model->upload_rules();
        $system_allowed_types=$upload['upload_mime'];
        $allowedtypes=array();
        $maxSize=$upload['upload_max_size']; 
        
        $typecheck = 0;
        $imagecheck = 0;
        $sizecheck = 0;
        $sizeval = 0;

        foreach($system_allowed_types as $key=>$system_allowed_type){
            
            array_push($allowedtypes,$system_allowed_type);
            
        }


            foreach ($myfiles as  $row) {

                    if (in_array($row['type'], $allowedtypes)) {

                        $typeval = 0;
                    } else {
                        $typeval = 1;

                    }

                    $typecheck += $typecheck + $typeval;

            }

        
            foreach($myfiles as $myfile){
                 $fileSize = $maxSize * 1048576;
                 if ($myfile["size"] <= $fileSize) {

                    $sizecheck = 0;
                } else {

                    $sizecheck = 1;
                }
            }

            if ($typecheck == 0 && $imagecheck == 0 && $sizecheck == 0) {

                if (!empty($file_name)) {

                    $file_name = strtolower($file_name);
                } else {

                    $file_name = 'file';
                }

                $product_name_cleaned = $this->my_model->clean($file_name);

                
                
                //upload function start	          
                $imgcount = count($_FILES[$fileName]['name']);
                if (isset($_FILES[$fileName]['name'])) {
                    if ($_FILES[$fileName]['name']) {

                        $config['upload_path'] = 'media_files/';


                        /*
                         * allowed_types in config
                         */
                        $config_allowedtypes = '';
                        $numExtension = count($system_allowed_types);
                        $i = 0;
                        foreach ($system_allowed_types as $syskey=>$system_allowed_type) {
                            
                            if (++$i === $numExtension) {
                                
                                $config_allowedtypes .= $syskey;
                                
                            } else {

                                $config_allowedtypes .= $syskey . '|';
                            }
                        }
      
                        
                        $config['allowed_types'] = $config_allowedtypes; // by extension, will check for whether it is an image
                        $config_fileSize = $maxSize * 1024;
                        $config['max_size'] = (int) $config_fileSize; // in kb 2048
                        $config['overwrite'] = FALSE;
                        $config['max_filename'] = 0;
                        $config['encrypt_name'] = FALSE;
                        $config['remove_spaces'] = TRUE;

                        $this->load->library('upload', $config);
                        $this->load->library('Multi_upload');
                        $filess = $this->multi_upload->go_upload($fileName, $product_name_cleaned);

                        if (!$filess) {
                            echo '***';
                        } else {



                            $data = array('upload_data' => $filess);

                             $file_names = '';
                                foreach ($filess as $img) {
                                    $file_names .= $img['name'] . ",";
                                }

//                            $file_names = substr($file_names, 0, -1);
                            echo $file_names;
                        }
                    }
                }

                //End of upload function
            }    
            
            
            
            
            
    }
    
    
    function diverse_array($vector) {
 
        $result = array();
        foreach ($vector as $key1 => $value1)
            foreach ($value1 as $key2 => $value2)
                   $result[$key2][$key1] = $value2;
        return $result; 
           
    }

    function clean($string) {
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
    
    
    function remove_upload_file(){
        
        $filename = $this->input->post('filename');
        $files = $this->input->post('files');
        $path='media_files/'.$filename;
        unlink($path);
        $return_files="";

        if (isset($files)) {
                $files=explode(',',$files);
                
               if (($key = array_search($filename, $files)) !== false) {
                        unset($files[$key]);
                } 
         $return_files= implode(",",$files);      
        }  
        
        echo $return_files;
        
    }
    
    
    
    function fileuploadnames(){
      return  array('media_category_image'=>"media_category_1",
                    'media_item_image'=>"media_item_1" );
    }
            

    function unique_identity($table, $conditional_array,$id) {
        $this->db->where($conditional_array);
        if(!empty($id)){
          $this->db->where('id !=', $id);
        }
        $query = $this->db->get($table);
        if ($query->num_rows() >= 1) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
   function getCategoryList($mediaTypeid) {
       
       $conditional_array=array("media_type"=>$mediaTypeid,"parent_id"=>0 );
       $extra_parameter_array=array();
       $fetchCategoryData=$this->my_model->ResultData("ms_category", "order_no", "ASC", $conditional_array,$extra_parameter_array);
       
       if(!empty($fetchCategoryData)){
           foreach($fetchCategoryData as $fetchCategoryDataRow){
                 $this->category_array[] = array('category' => $fetchCategoryDataRow->category,
                    'id' => $fetchCategoryDataRow->id,
                    'parent_id' => $fetchCategoryDataRow->parent_id);
                $this->my_model->getSubCategoryList($fetchCategoryDataRow->id, $mediaTypeid);  
           }
       }
        return $this->category_array;
    }

    
    function getSubCategoryList($cat_id, $mediaTypeid, $dashes = '') {

        $dashes .= '__';
        
        $conditional_array=array("media_type"=>$mediaTypeid,"parent_id"=>$cat_id);
        $extra_parameter_array=array();
        $fetchCategoryData=$this->my_model->ResultData("ms_category", "order_no", "ASC", $conditional_array,$extra_parameter_array);
       
        if(!empty($fetchCategoryData)){
            foreach($fetchCategoryData as $fetchCategoryDataRow){
                  $this->category_array[] = array('category' => $dashes.$fetchCategoryDataRow->category,
                     'id' => $fetchCategoryDataRow->id,
                     'parent_id' => $fetchCategoryDataRow->parent_id);
                 $this->my_model->getSubCategoryList($fetchCategoryDataRow->id, $mediaTypeid,$dashes);  
            }
        }
    }

    
    
    function tree_creation($parent_id, $current_id, $typeCheck) {

        ini_set('max_execution_time', 0);
        $parent_cat_result = $this->my_model->get_first_parent($parent_id,$typeCheck);
        
        if ($typeCheck == "media_category") {
            $current_field = $this->my_model->RowData('ms_category', $current_id, 'id');
        } else if ($typeCheck == "product_category") {
            $current_field = $this->my_model->RowData('ms_category', $current_id, 'id');
        }


        if ($current_field->parent_id == 0 || $typeCheck == 'product' || $typeCheck == 'media') {
            $current_ids = '';
            $current_names = '';
            $current_slugs = '';
            $current_full = '';
        } else {
            $current_ids = '+' . $current_field->id;
            $current_names = '+' . strtolower($current_field->category);
            $current_slugs = '+' . $current_field->identity;
            $current_full = $current_ids . '_' . $current_names . '_' . $current_slugs;
        }



        $parent_cat_result = explode('___', $parent_cat_result);
        $parent_cat_splited = explode('**', $parent_cat_result[0]);
        $cat_parent_id = $parent_cat_splited[0];
        $cat_parent_name = $parent_cat_splited[1];
        $cat_parent_route = $parent_cat_splited[2];

        $parent_cat_splited2 = explode('**', $parent_cat_result[1]);

        $category_ids = $parent_cat_splited2[0];
        $category_ids = explode('+', $category_ids);
        $category_ids = array_filter($category_ids);
        $category_ids = array_unique($category_ids);
        $category_ids = implode('+', $category_ids);
        $category_ids = $current_ids . '+' . $category_ids . '+';


        $category_names = $parent_cat_splited2[1];
        $category_names = explode('+', $category_names);
        $category_names = array_filter($category_names);
        $category_names = array_unique($category_names);
        $category_names = implode('+', $category_names);
        $category_names = $current_names . '+' . $category_names . '+';


        $category_slugs = $parent_cat_splited2[2];
        $category_slugs = explode('+', $category_slugs);
        $category_slugs = array_filter($category_slugs);
        $category_slugs = array_unique($category_slugs);
        $category_slugs = implode('+', $category_slugs);
        $category_slugs = $current_slugs . '+' . $category_slugs . '+';

        $category_full = $parent_cat_splited2[3];
        $category_full = explode('+', $category_full);
        $category_full = array_filter($category_full);
        $category_full = array_unique($category_full);
        $category_full = implode('+', $category_full);
        $category_full = $current_full . '+' . $category_full . '+';



        $tree_arr = array('category_ids' => $category_ids,
            'category_names' => $category_names,
            'category_slugs' => $category_slugs,
            'category_full' => $category_full,
            'cat_parent_id' => $cat_parent_id,
            'cat_parent_name' => $cat_parent_name,
            'cat_parent_route' => $cat_parent_route,
        );
        return $tree_arr;
    }

    
    
    
       function get_first_parent($cid,$typeCheck) {
        ini_set('max_execution_time', 0);
        $j = '';
        $i = $cid;
        $catids = '';
        $catnames = '';
        $catslugs = '';
        $catfull = '';
        while ($i > 0) {
            $this->db->where('id', $i);
            if ($typeCheck == "media_category") {
                $category = $this->db->get('ms_category')->row();
            }else if($typeCheck == "product_category"){
                $category = $this->db->get('sms_category')->row();
            }
            
            $i = $category->parent_id;

            $catids .= $category->id . '+';
            $catnames .= strtolower($category->category) . '+';
            $catslugs .= $category->identity . '+';

            $catfull .= $category->id . '__' . strtolower($category->category) . '__' . $category->identity . '+';

            if ($i > 0) {
                $j = $category->parent_id . '**' . $category->category . '**' . $category->identity;

                $catids .= $category->parent_id . '+';
                $catnames .= strtolower($category->category) . '+';
                $catslugs .= $category->identity . '+';

                $catfull .= $category->id . '__' . strtolower($category->category) . '__' . $category->identity . '+';
            } else
            if ($i == '0') {
                $j = $category->id . '**' . $category->category . '**' . $category->identity;

                $catids .= $category->id . '+';
                $catnames .= strtolower($category->category) . '+';
                $catslugs .= $category->identity . '+';

                $catfull .= $category->id . '__' . strtolower($category->category) . '__' . $category->identity . '+';
            }
        }

        $alldata = $catids . '**' . $catnames . '**' . $catslugs . '**' . $catfull;
        return $j . '___' . $alldata;
    } 
    
    
    function get_image_data($parameter_array,$need_type){
         $column=$parameter_array["column"];
         $this->db->where("id",$parameter_array["id"]);
         $this->db->where("media_type",$parameter_array["type"]);
         $query = $this->db->get($parameter_array["table"])->row();
         $image_row=$query->$column;
         
         $image_json_row=json_decode($image_row,TRUE);
         if($need_type=="count"){
            return count($image_json_row);  
         }else if($need_type=="data"){
            return $image_json_row;
         }
        
    }
    
    
    function gallery_need_data() {

        $image_type="";
        $column = "";
        $table = "";
        $id = "";
        $type = "";
        $type_name = "";
        
        if (isset($_GET['image_type'])) {
            $image_type = $_GET['image_type'];

            switch ($image_type) {
                case "media_category_image":
                    $column = "category_file";
                    $table = "ms_category";
                    break;
                case "media_item_image":
                    $column = "media_file";
                    $table = "ms_medias";
                    break;
            }
        }

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        }

        if (isset($_GET['type'])) {
            $type = $_GET['type'];
            $media_types = $this->my_model->media_types();
            $type_name = $media_types[$type]['name'];
        }
        
        
       return array("image_type"=>$image_type,
           "column"=>$column,
           "table"=>$table,
           "id"=>$id,
           "type"=>$type,
           "type_name"=>$type_name
        );
    }
    
    
    
    
    function update_image_gallery_item($id,$column,$table){
        $image_row=$this->my_model->RowData($table, $id, "id");
        $image_column=$image_row->$column;
        $image_column= json_decode($image_column,TRUE);

        $title=$this->security->xss_clean($this->input->post('title'));
        $position=$this->security->xss_clean($this->input->post('position'));
        $upload_image_1=$this->security->xss_clean($this->input->post('upload_image_1'));
        $upload_image= json_decode($upload_image_1,TRUE);
        if(empty($upload_image)){
          $filename=$image_column[$position]['filename'];
        }else{
           $filename=$upload_image[0]['filename']; 
        }
        $image_column[$position]['filename']=$filename;
        $image_column[$position]['title']=$title;
        
        $data=array($column=>json_encode($image_column));

        $this->db->where('id', $id);
        $this->db->update($table, $data);
    }
    
    
    
    function sort_array(){
        $sort_array = array('ASC' => 'ascending', 'DESC' => 'descenting', 'RANDOM' => 'random');
        return $sort_array;
    }
    function custom_sort_array(){
        $custom_sort_array = array('order_no' => 'order', 'id' => 'id');
        return $custom_sort_array;
    }
    function custom_sort_array_no_order(){
        $custom_sort_array = array( 'id' => 'id');
        return $custom_sort_array;
    }
    function item_status_array(){
        $item_status_array = array('a' => 'Active', 'd' => 'Deactive');
        return $item_status_array;
    }

}