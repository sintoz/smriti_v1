<?php $this->load->view('above_footer'); ?>
<script src="<?php echo base_url() . 'assets/web_end/' ?>js/popper.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?php echo base_url() . 'assets/web_end/' ?>js/bootstrap.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?php echo base_url() . 'assets/web_end/' ?>js/custom.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?php echo base_url() . 'assets/web_end/' ?>js/wow.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script>
    new WOW().init();
</script>
<script src="<?php echo base_url() . 'assets/web_end/' ?>assets/owlcarousel/owl.carousel.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>

<script  src="<?php echo base_url() . 'assets/web_end/' ?>js/script.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>

<script src="<?php echo base_url() . 'assets/web_end/' ?>js/jquery.checkboxall-1.0.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script>
    $('.pr-filt-1').checkboxall();
    $('.pr-filt-2').checkboxall();
    $('.pr-filt-3').checkboxall();
</script> 
<script src="<?php echo base_url() . 'assets/web_end/' ?>js/giffy.js"></script>
<script>
  $('.giffy-img').giffy();
</script>

</body>
</html>