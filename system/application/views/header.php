<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo $page_meta_description;?>"/>
<meta name="keywords" content="<?php echo $page_meta_keywords;?>"/>

<title><?php echo $page_meta_title;?></title>


<link rel="shortcut icon" href="<?php echo base_url()?>assets/web_end/images/fav.png" type="image/x-icon">
<link rel="icon" href="<?php echo base_url()?>assets/web_end/images/fav.png" type="image/x-icon">

<link href="<?php echo base_url().'assets/web_end/'?>css/bootstrap.min.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/web_end/'?>css/stylesheet.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/web_end/'?>css/responsive.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet">
<link href="<?php echo base_url().'assets/web_end/'?>css/smartphoto.css?<?php echo date('l jS \of F Y h:i:s A'); ?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url().'assets/web_end/'?>assets/owlcarousel/assets/owl.carousel.min.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/web_end/'?>assets/owlcarousel/assets/owl.theme.default.min.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">



<script src="<?php echo base_url() . 'assets/web_end/' ?>js/jquery.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>

<!--developer section-->


<script src="<?php echo base_url().'assets/ajaxupload/'?>js/ajaxloader.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script src="<?php echo base_url().'assets/ajaxupload/'?>js/jquery.validate.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>

<link rel="stylesheet" href="<?php echo base_url().'assets/developer_files/'?>css/custom.css?<?php echo date('l jS \of F Y h:i:s A'); ?>">
    
<script src="<?php echo base_url().'assets/developer_files/'?>js/custom.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>

<!--EOF developer section-->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-181790805-1">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-181790805-1');
</script>


</head>



<body>
    <input type="hidden" value="<?php echo base_url();?>" class="base_url">
   <?php $this->load->view('below_header');?>
