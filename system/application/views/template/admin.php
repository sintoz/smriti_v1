<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Admin Panel</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />

         <?php $this->load->view("admin_layout/headerscript"); ?>
        
        <!-- END THEME LAYOUT STYLES -->
        <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <?php $this->load->view("admin_layout/headerpage"); ?>
 
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                 <?php $this->load->view("admin_layout/side_navigation"); ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">


                        <!-- END PAGE HEADER-->
                        <!-- BEGIN DASHBOARD STATS 1-->

                             <?php echo $contents; ?> 
                        <input type="hidden" name="base_url" class="base_url" value="<?php echo base_url();?>">
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->

                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->
            </div>
            <!-- END CONTAINER -->
        </div>  
        <!-- BEGIN CORE PLUGINS -->
         <?php $this->load->view("admin_layout/footerscript"); ?>
         <?php $this->load->view("admin_layout/alertnotify"); ?>
    </body>

</html>