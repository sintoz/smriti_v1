<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url() ?>assets/adminpanel/fonts/OpenSans.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="<?php echo base_url() ?>assets/adminpanel/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->

     <link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/adminpanel/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/adminpanel/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="<?php echo base_url() ?>assets/adminpanel/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/developer_files/css/notifIt.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/developer_files/css/admin_custom.css" rel="stylesheet" type="text/css" />
<script src="<?php echo base_url() ?>assets/adminpanel/global/plugins/jquery.min.js" type="text/javascript"></script>

