<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
          <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                       <?php
            $media_types = $this->my_model->media_types();
            if (!empty($media_types)) {
                $type="";
                 if(!empty($_GET['type'])){
                    $type= $_GET['type'];   
                  }
                foreach ($media_types as $media_key=>$media_type_item) {
                   
                    ?>
                            <li class="nav-item  <?php if($type==$media_key){ echo "active";}?> ">
                            <a href="javascript:void(0);" class="nav-link nav-toggle">
                                    <i class="<?php echo $media_type_item['nav_icon']; ?>"></i>
                                    <span class="title"><?php echo ucwords($media_type_item['name']); ?> Management</span>
                                    <span class="arrow"></span>
                                </a>
                              <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="<?php echo base_url().'media_admin/category_management?type='.$media_key?>" class="nav-link ">
                                            <span class="title">Add <?php echo ucwords($media_type_item['name']); ?> Category</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?php echo base_url().'media_admin/view_category?type='.$media_key?>" class="nav-link ">
                                            <span class="title">View <?php echo ucwords($media_type_item['name']); ?> Category</span>
                                        </a>
                                    </li>
                                     <li class="nav-item">
                                <a href="<?php echo base_url().'media_admin/media_management?type='.$media_key?>" class="nav-link ">
                                    <span class="title">Add <?php echo ucwords($media_type_item['name']); ?>s</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url().'media_admin/view_media?type='.$media_key?>" class="nav-link ">
                                    <span class="title">View <?php echo ucwords($media_type_item['name']); ?> Media</span>
                                </a>
                            </li>
                              </ul>                            
                            </li>
                         <?php
                }
            }
            ?>  
                            <li class="nav-item">
                                <a href="javascript:void(0);" class="nav-link nav-toggle">
                                    <i class="fa fa-shopping-cart"></i>
                                    <span class="title">Order Management</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href=" <?php echo base_url().'order_admin/view_order'?>" class="nav-link ">
                                            <span class="title">View Orders</span>
                                        </a>
                                    </li>               
                                </ul>               
                            </li> 

        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>