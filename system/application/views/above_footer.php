<div class="flash_message" style="display:none;"></div>
<footer class="footer-sec wow fadeIn sa_removethis">

    <div class="footer-top">
        <div class="container">
            <div class="footer-widgets">

                <div class="row">

                    <div class="col-lg-3">
                        <div class="footer-logo wow fadeIn"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/footer-logo-smritidesign.svg" alt=""/></div>
                    </div><!--col-lg-3-->

                    <div class="col-lg-9">

                        <div class="row">

                            <div class="col-md-4">
                                <div class="footer-column wow fadeIn">
                                    <h5>Get in Touch</h5>
                                    <div class="txt">
                                        Smriti Design<br>
                                        Pallikulam Road,<br>
                                        Thrissur-680 001,<br>
                                        Kerala, India
                                    </div>
                                    <div class="txt-contact">
                                        <div class="left">T:</div>
                                        <div class="right">+91-487 242 1229</div>
                                    </div>
                                    <div class="txt-contact">
                                        <div class="left">M:</div>
                                        <div class="right">+91-944 640 1229</div>
                                    </div>
                                    <div class="txt-contact">
                                        <div class="left">E:</div>
                                        <div class="right"><a href="mailto:smritidesign@yahoo.com">smritidesign@yahoo.com</a></div>
                                    </div>
                                </div>
                            </div><!--col-md-4-->

                            <div class="col-md-3">
                                <div class="footer-column wow fadeIn">
                                    <h5>Quick Links</h5>
                                    <ul class="links">
                                        <li><a href="<?php echo base_url(); ?>">Home</a></li>
                                        <li><a href="<?php echo base_url() ?>about">About Us</a></li>
                                        <li><a href="<?php echo base_url() ?>portfolio">Portfolio</a></li>
                                        <li><a href="<?php echo base_url(); ?>careers">Careers</a></li>
                                        <li><a href="<?php echo base_url() ?>contact">Contact Us</a></li>
                                        <li><a href="<?php echo base_url() ?>privacy-poilcy">Privacy Poilcy</a></li>
                                        <li><a href="<?php echo base_url() ?>terms-and-conditions">Terms & Conditions</a></li>
                                        <li><a href="<?php echo base_url(); ?>payment-policy">Payment Policy</a></li>
                                    </ul>
                                </div>
                            </div><!--col-md-3-->

                            <div class="col-md-3">
                                <div class="footer-column wow fadeIn">
                                    <h5>Services</h5>
                                    <ul class="links">
                                        <?php
                                        if (!empty($services)) {
                                            $i = 1;
                                            foreach ($services as $serkey => $service) {
                                                ?>
                                                <li><a href="<?php echo base_url() . 'services?service_id=' . $serkey; ?>"><?php echo ucwords($service['name']); ?></a></li>
                                                <?php
                                                $i++;
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div><!--col-md-3-->
                            <div class="col-md-2">
                                <div class="footer-column wow fadeIn">
                                    <h5>Follow Us</h5>
                                    <div class="social-icon">
                                        <a href="https://www.instagram.com/smritidesign" target="_blank"><span class="icon-instagram iconstyle"></span></a>
                                        <a href="https://www.facebook.com/SmritiDesign" target="_blank"><span class="icon-facebook iconstyle"></span></a>
                                        <a href="https://twitter.com/smritidesigns" target="_blank"><span class="icon-twitter iconstyle"></span></a>
                                        <a href="https://youtube.com/channel/UC5oeR-e2jwjPoOoParIelXA" target="_blank"><span class="icon-youtube iconstyle"></span></a>
                                    </div>
                                </div>
                            </div><!--col-md-2-->


                        </div><!--row-->

                    </div><!--col-lg-9-->

                </div><!--row-->

            </div><!--footer-widgets-->
        </div><!--container-->
    </div><!--footer-top-->

    <div class="footer-bottom wow fadeIn">
        <div class="container">
            <div class="copyright">Copyright <span>&copy;</span> <?php echo date('Y'); ?> smritidesign. All Rights Reserved.</div>
        </div><!--container-->
    </div><!--footer-bottom-->

</footer>


<a href="#" class="cd-top js-cd-top">Top</a>
