<div class="form-group">
    <label class="control-label"><?php echo $label;?></label>
    <input type="file" name="<?php echo $name;?>[]"
           data-attachment_count="<?php echo $attachment_count;?>"
           data-upload_size="<?php echo $upload['upload_max_size'];?>"
           data-input_name="<?php echo $name;?>"
           class="form-control sa_file_upload" <?php echo $count;?>/>
    
    
    <input type="hidden" class="sa_form_type_<?php echo $attachment_count;?>" value="<?php echo $form_type;?>">
    <input type="hidden" name="<?php echo $post_name;?>" id="final_image_post_<?php echo $attachment_count;?>">
    <input type="hidden" class="sa_attach_identity" data-attach_identity="<?php echo $attachment_count;?>">
    <input type="hidden" class="sa_final_images_<?php echo $attachment_count;?>">
    <input type="hidden" class="sa_file_input_name_<?php echo $attachment_count;?>" name="file_input_name" >
    <input type="hidden" class="sa_img_extension_<?php echo $attachment_count;?>" value='<?php echo json_encode($this->my_model->imag_extension_array());?>'>
    
    <span class="sa_error sa_error_<?php echo $attachment_count;?>"></span>
    <span class="sa_progress sa_progress_<?php echo $attachment_count;?> hide">
        <img src="<?php echo base_url().'assets/ajaxupload/images/image_upload.gif'?>">
    </span>
</div>
 <div class="row sa_preview sa_preview_<?php echo $attachment_count;?> margin-bottom-10"></div>
<?php if ($form_type=="edit") { ?>
 
 <a href="<?php echo base_url().'general_admin/media_image_gallery?'. $_SERVER['QUERY_STRING'].'&image_type='.$image_type ?>" class="btn btn-circle red btn-outline" target="_blank">View Image Gallery - Update</a>
<?php }
?>
<?php if ($form_type=="update_gallery") { 
    $file=$data_row['filename'];
    ?>
    <div class="tile-body">
         <img src="<?php echo base_url() . 'media_files/' . $file; ?>" height="150"> 
    </div>
<?php } ?>
