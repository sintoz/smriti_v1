        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> <?php echo ucwords(str_replace("_"," ",$type_name));?> </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="note note-success">
                                    <p> Drag the images and save rearrange the order </p>
                                    <p class="sa_error"> NB : first order image always default image</p>
                                </div>
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-image"></i>Image Gallery</div>
                                            <a href="javascript:void(0);" class="btn btn-danger pull-right sa_update_order">Update The Order</a>
                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead class="flip-content">
                                                <tr>
                                                    <th class="text-center" width="10%">ID </th>
                                                    <th class="text-center" width="20%">Image</th>
                                                    <th class="text-center">Title</th>
                                                    <th class="text-center" width="20%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody class="sa_sortable">
                                               
                                                    <?php
                                                if (!empty($data_list)) {
                                                    foreach ($data_list as $key => $data) {
                                                        ?>
                                                        <tr class="sa_image_item" data-img_name="<?php echo $data['filename'] ?>" data_image_title="<?php echo $data['title'] ?>" >
                                                            <td class="text-center"><?php echo $key + 1; ?></td>
                                                            <td class="text-center"><img src="<?php echo base_url() . 'media_files/' . $data['filename'] ?>" width="100" height="100"></td>
                                                            <td class="text-center"><?php echo $data['title'] ?></td>  
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions<i class="fa fa-angle-down"></i></button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                <li>
                                                                    <a href="<?php echo base_url().'general_admin/update_image_gallery_item?'.$_SERVER['QUERY_STRING'].'&position='.$key; ?>">
                                                                        <i class="fa fa-edit"></i>Edit</a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0);"
                                                                      onclick="removeitem('<?php echo base_url().'general_admin/removeitem?'.$_SERVER['QUERY_STRING'].'&position='.$key;?>')">
                                                                        <i class="fa fa-trash-o" ></i> Delete </a>
                                                                </li>
                                                            </ul>
                                                                </div>
                                                            </td>      
                                                                    
                                                                    
                                                                    
                                                        </tr>
                                                    <?php }
                                                }
                                                ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
<script>
    $('body').on('click','.sa_update_order',function(){
        ajaxindicatorstart("Please wait");
        var final_image_post_array =[];
            $(".sa_image_item").each(function(){

                var filename = $(this).attr("data-img_name");
                var title =  $(this).attr("data_image_title");

                final_image_post_array.push({filename: filename,
title: title});
            });



                var base_url=$(".base_url").val();
                $.ajax({
                    url: base_url+"general_admin/update_image_order?<?php echo $_SERVER['QUERY_STRING'];?>",
                    data: {updated_order: final_image_post_array},
                    type: "POST",
                    success: function ()
                    {

                        ajaxindicatorstop();
                        location.reload();

                    }
                });
            });

</script>