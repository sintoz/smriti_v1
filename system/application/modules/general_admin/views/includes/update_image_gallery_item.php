
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<h1 class="page-title">
    <?php
    echo 'EDIT - '.ucwords($type_name);
    ?><small> Image</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- CHANGE PASSWORD TAB -->
        <form action="<?php echo base_url().'general_admin/update_image_gallery_item?'. $_SERVER['QUERY_STRING'];?>" method="post" enctype='multipart/form-data' class="sa_multiple_upload_form" id="sa_multiple_upload_form">
            <input type="hidden" class="sa_mediatype" name="mediatype" value="<?php echo $type; ?>">
            <input type="hidden" class="sa_mediatype" name="position" value="<?php echo $position; ?>">

            <div class="form-group">
                <label class="control-label">Image Title</label>
                <input type="text" name="title"
                       class="form-control" value="<?php echo $data_row['title'];?>" /> 
                <span class="sa_error"><?php echo form_error('title'); ?></span>
            </div>
            
            <?php 
            $fileuploadnames=$this->my_model->fileuploadnames();
            $attachment_data['label']="Image"; // file label
            $attachment_data['name']="upload_image"; // file input name
            $attachment_data['post_name']="upload_image_1"; // attachment save field [post data]
            $attachment_data['count']="";  // multiple upload or single upload
            $attachment_data['attachment_count']=$fileuploadnames[$image_type]; // form attachment count
            $attachment_data['image_type']=$image_type; // image type
            $attachment_data['form_type']=$form_type; // form type
            $attachment_data['upload']=$this->my_model->upload_rules();// upload rules array

            $this->load->view('attachment_view/attachment',$attachment_data);
            ?>

            <div class="margin-top-10">
                <button type="button" class="btn green pull-right sa_attach_submit "> Save </button>
            </div>
        </form>
    </div>

</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->