<?php 
$category_items="<option value=''>--Select Category--</option>"
        . "<option value='0' ".$parentselected." class='".$hideShowClass."'>Parent Category</option>";
if(!empty($categoryArray)){
    foreach($categoryArray as $category_row){ 
        $parent_selected="";
        $parent_disabled="";
        if($type === "media_data"){
            if($parentid===$category_row['id']){
                $parent_selected= " selected ";
            }
        }else if($type === "media_category"){
            if($category_row['id']==$id){
                $parent_disabled = " disabled "; 
            }   
            if($parentid==$category_row['id']){
             
                $parent_selected= " selected ";
            }
        }

        $child_disabled="";
        if ($type == "media_data") {
            $conditional_array=array("parent_id"=>$category_row['id']);
            $extra_parameter_array=array();
            $ChildData=$this->my_model->ResultData("ms_category", "id", "ASC", $conditional_array,$extra_parameter_array);
            if (!empty($ChildData)) {
                $ChildDataCount = count($ChildData);
                if ($ChildDataCount > 0) {
                    $child_disabled = " disabled ";
                }
            }
        }

        $category_items.= "<option value='".$category_row['id']."' ".  $parent_selected." "
                . "".$parent_disabled."  ".$child_disabled." >".$category_row['category']."</option>";
 
  }
}
echo $category_items;