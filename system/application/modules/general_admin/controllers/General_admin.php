<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author sms
 */
class General_admin extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('panel_model');
        $this->load->library('ion_auth');
        $this->load->library('encryption');


        if ($this->session->userdata('logged_adminpanel') != 'true') {
            redirect('stablemode/index');
        }
    }

    function media_image_gallery(){
        $receive_data=$this->my_model->gallery_need_data();  
        $data['type_name']=$receive_data['type_name'];   
        $parameter_array=array("id"=>$receive_data['id'],"type"=>$receive_data['type'],"table"=>$receive_data['table'],"column"=>$receive_data['column']);
        $data['data_list']=$this->my_model->get_image_data($parameter_array,"data");
        $this->template->load('admin', 'general_admin/includes/view_image_gallery', $data);
    
    }
    
    
    
    function update_image_gallery_item(){
        $data['position']=$position=$_GET['position'];
        $receive_data=$this->my_model->gallery_need_data();  
        $parameter_array=array("id"=>$receive_data['id'],"type"=>$receive_data['type'],"table"=>$receive_data['table'],"column"=>$receive_data['column']);
        $data_list=$this->my_model->get_image_data($parameter_array,"data");
        $data['data_row']=$data_row=$data_list[$position];
        
        $data['type_name']=$receive_data['type_name']; 
        $data['image_type']=$image_type=$receive_data['image_type']; 
        $data['type']=$type=$receive_data['type']; 
        $id=$receive_data['id']; 
        $table=$receive_data['table']; 
        $column=$receive_data['column'];
        
        $data['form_type']="update_gallery";
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('position', 'Position', 'required');

        if ($this->form_validation->run($this) == FALSE) {

            $this->template->load('admin', 'includes/update_image_gallery_item',$data);
            
        } else {
            
            $this->my_model->update_image_gallery_item($id,$column,$table);
            $this->session->set_flashdata('message', "Updated Successfully!..");
            redirect('general_admin/media_image_gallery?type='.$type.'&id='.$id.'&image_type='.$image_type);
        }
    }
    
   
   function removeitem(){
       $position=$_GET['position'];
       if(!empty($position)){
           
            $receive_data=$this->my_model->gallery_need_data();
            
            $table= $receive_data['table']; 
            $id= $receive_data['id'];  
            $column=$receive_data['column'];
            $type=$receive_data['type']; 
            $image_type=$receive_data['image_type']; 
            
            $image_row=$this->my_model->RowData($table, $id, "id");
            $image_column=$image_row->$column;
            $image_column= json_decode($image_column,TRUE);
            
            unset($image_column[$position]);
            
            $data=array($column=>json_encode($image_column));

            $this->db->where('id', $id);
            $this->db->update($table, $data);
       }
       $this->session->set_flashdata('message', "Removed Successfully!..");
       redirect('general_admin/media_image_gallery?type='.$type.'&id='.$id.'&image_type='.$image_type);
   } 
    
    
   function update_image_order(){
       $image_column=$this->security->xss_clean($this->input->post('updated_order'));
       if(!empty($image_column)){
            $receive_data=$this->my_model->gallery_need_data();
            
            $table= $receive_data['table']; 
            $id= $receive_data['id'];  
            $column=$receive_data['column'];

            $data=array($column=>json_encode($image_column));

            $this->db->where('id', $id);
            $this->db->update($table, $data);
       }
       
   }
   
    
}
