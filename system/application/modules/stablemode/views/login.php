        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?php echo base_url().'stablemode/index/' ;?>" method="post">
                <h3 class="form-title">Login to your account</h3>
                
                <?php if($this->session->flashdata('message')){ ?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <strong>Login Error !</strong> <?php echo $this->session->flashdata('message');?>
                </div>
                <?php } ?>
                
                
                
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" 
                               autocomplete="off" placeholder="Username"
                               name="username" /> </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix"
                               type="password" autocomplete="off" placeholder="Password"
                               name="password" /> </div>
                </div>
                <div class="form-actions">

                    <button type="submit" class="btn green pull-right"> Login </button>
                </div>
            </form>
            <!-- END LOGIN FORM -->

        </div>
        <!-- END LOGIN -->