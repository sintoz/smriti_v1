<?php

class Stable_model extends CI_Model {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    function login() {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        $username= $this->security->xss_clean($username);
        $password= $this->security->xss_clean($password);

        $this->db->where('username', $username);
        $admin_details = $this->db->get('admin')->row();
        
        $msg1 = $admin_details->password;
        $encrypted_string1 = $this->encryption->decrypt($msg1);


        if ($encrypted_string1 == $password && $username == $admin_details->username) {
            return 1;
        } else {
            return 0;
        }
    }

}
