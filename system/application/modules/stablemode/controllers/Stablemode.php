<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author sms
 */
class Stablemode extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('stable_model');
        $this->load->library('encryption');
    }

    public function index() {
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {

            if ($this->session->userdata('logged_adminpanel') == 'true') {

                $log_usernames = $this->session->userdata('logged_username');
                $this->db->where('username', $log_usernames);
                $loged_details = $this->db->get('admin')->row();
                $loged_type = $loged_details->type;

                $this->session->set_userdata('logged_id', $loged_details->id);

                if ($loged_details->type == 'super') {

                    redirect('admin/home');
                } 
                
            } else {

                $this->template->load('authenticate', 'login');
            }
        } else {
            $result = $this->stable_model->login();
            if ($result > 0) {

                $username = $this->input->post('username');
                $this->session->set_userdata('logged_adminpanel', 'true');
                $this->session->set_userdata('logged_username', $username);


                $this->db->where('username', $username);
                $loged_details = $this->db->get('admin')->row();
                $loged_type = $loged_details->type;

                $this->session->set_userdata('logged_id', $loged_details->id);

                if ($loged_details->type == 'super') {

                    redirect('admin/home');
                } 
            } else {
                $this->session->set_flashdata('message', 'Please Enter Valid Username And password');
                redirect('stablemode/index');
            }
        }
    }

    function logout() {
        $this->session->set_userdata('logged_adminpanel', 'false');
        $this->session->set_userdata('logged_username', '');
        $this->session->set_userdata('logged_id', '');
        redirect('stablemode/index');
    }

}
