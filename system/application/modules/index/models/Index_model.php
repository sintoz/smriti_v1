<?php
class Index_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
    }



    function order_creation() {
        $mediatype = $this->security->xss_clean($this->input->post('media_type'));
        $category_id = $this->security->xss_clean($this->input->post('category_id'));
        $name = $this->security->xss_clean($this->input->post('name'));
        $mobile = $this->security->xss_clean($this->input->post('mobile'));
        $email = $this->security->xss_clean($this->input->post('email'));
        $requiremnt = $this->security->xss_clean($this->input->post('requiremnt'));
        
        
            $data = array(
                'user' => $name,
                'email' => $email,
                'phone' => $mobile,
                'user_message' => $requiremnt,
                'type' => $mediatype,
                'category_id' => $category_id,
            );
        
        $this->db->insert('sms_orders', $data);
        $id = $this->db->insert_id();
        
        
         $data2 = array(
                'order_id' => "SMD-".$id
            );
         $this->db->where('id', $id);
         $this->db->update('sms_orders', $data2);
    }
    
    
    function load_more_data($perpage){
        $last_id=$this->input->post('last_id');
        if (!empty($last_id)) {
//             $this->db->limit($perpage,$last_id);
        }
        if (!empty($_POST['hashes'])) {
            $hashes = $_POST['hashes'];
            
            if(!empty($hashes)){
                foreach($hashes as $hash){
                    
                    if(strpos($hash, "service=") !== false){
                        
                        $hash = str_replace("service=", "", $hash);
                        $value_array = explode('~', $hash);
                        array_shift($value_array);
                        array_pop($value_array);
                        $like_clause_string = '';
                        $l = 1;
                        foreach ($value_array as $value_key => $value_row) {

                                 $value_row = '+' . $value_row . '+';
                                 $like_clause_string .= "categoryidtree LIKE '%" . $value_row . "%'";
                                 if (count($value_array) > 1) {
                                    if ($l < count($value_array)) {
                                        $like_clause_string .= ' OR ';
                                    }
                                 }
                            $l++;
                        }
                        $this->db->where("(" . $like_clause_string . ")", NULL, FALSE);
                    } 
                    
                }
            }
        }
        
        if (empty($_SERVER['QUERY_STRING']) || empty($last_id)) {
//            $this->db->limit($perpage);
        }
        
        $this->db->where('media_status','a');
        $this->db->order_by('order_no','ASC');
        $query = $this->db->get("ms_medias");
         return $query->result();
    }

    

}