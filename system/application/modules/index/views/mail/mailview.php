<!doctype html>
<html> 
    <head>
        <meta charset="utf-8">
        <style type="text/css">
            body {
                margin: 0px;
                font-family: 'Roboto', sans-serif;
                font-weight:400;
                font-size:13px;
                color:#000;
            }

            * {
                box-sizing: border-box;
            }

            .wrapper{
                position:relative;
                padding:10px;
            }
            .title-box{
                position:relative;
                padding:0;
            }	

            .title-box .logo{
                margin:0 0 2px 0;
            }

            .title-box .logo img{
                max-width:130px;
            }

            .title-box h2{
                margin:0 0 5px 0;
                padding:0;
                font-size:16px;
                font-weight:700;	
            }



            .table {
                width: 100%;
                max-width: 100%;
                background-color: #fff;
                border-spacing: 0px;
            }

            .table th,
            .table td {
                padding: 7px;
                vertical-align: top;
                border-top: 0px solid #dee2e6;
            }

            .table thead th {
                vertical-align: bottom;
                border-bottom: 0px solid #dee2e6;
            }

            .table tbody + tbody {
                border-top: 2px solid #dee2e6;
            }

            .table .table {
                background-color: #fff;
            }

            .table-bordered {
                border: 0px solid #dee2e6;
            }

            .table-bordered th,
            .table-bordered td {
                border: 0px solid #dee2e6;
            }

            .table-bordered thead th,
            .table-bordered thead td {
                border-bottom-width: 2px;
            }

/*            .table-striped tbody tr:nth-of-type(even) {
                background-color: rgba(249, 249, 249, 1);
            }*/


            <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">	


        </style>
    </head>

    <body>

        <div class="wrapper">
            <div class="table-box" style="position:relative; background:#eeeeee; color:#000; padding:5px;max-width:500px; margin:auto;">

                <div class="title-box">
                    <h2><?php echo ucwords($name) . ' - ' .str_replace('_', ' ', ucfirst($form_type)); ?></h2>
                </div>

                <table class="table table-striped">

                    <tbody>
                        <?php
                        $form_value = $json_formdata;
                        foreach ($form_value as $key => $jsons) {
                            $number=0;
                            foreach ($jsons as $key => $value) {
                                ?>
                                <tr  <?php if($number % 2== 0){ ?>
                                            style=" background-color:#f9f9f9;"
                                      <?php  }?>>
                                    <td width="50%" height="40"><?php echo str_replace("_", " ", ucwords($key)); ?> </td>
                                    <td width="50%"><?php echo $value; ?></td>
                                </tr>
                                <?php
                                $number++;
                            }
                        }
                        ?>

                    </tbody>
                </table>

            </div>
        </div>

    </body>
</html>
