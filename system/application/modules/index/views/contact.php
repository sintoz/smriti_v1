<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2>Contact Us</h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Contact Us</li>
        </ol>
    </div>	
</section>

<section class="contact-sec">
    <div class="container">

        <div class="row">

            <div class="col-lg-4 col-md-5">

                <div class="box wow fadeInUp">
                    <div class="icon"><span class="icon-location iconstyle"></span></div>
                    <div class="txt">Smriti Design<br>
                                Pallikulam Road<br>
                                Thrissur-680 001<br>
                                Kerala, India</div>
                </div>

                <div class="box wow fadeInUp">
                    <div class="icon"><span class="icon-phone iconstyle"></span></div>
                    <div class="txt">
                        +91 487 242 1229<br>
                        +91 9446401229
                    </div>
                </div>

                <div class="box wow fadeInUp">
                    <div class="icon"><span class="icon-email iconstyle"></span></div>
                    <div class="txt mt">
                        <a href="mailto:smritidesign@yahoo.com">smritidesign@yahoo.com</a>
                    </div>
                </div>	

            </div>

            <div class="col-lg-8 col-md-7">

                <div class="contact-form wow fadeIn">
                    <h5>Feedback</h5>
                    <div class="desc">We value your feedback. Please complete the following form and help us improve.</div>

                    <form id="sa_contact_form" class="sa_contact_form sa_formdata_check">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control  sa_validation" name="name" placeholder="Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control sa_validation"  name="mobile" placeholder="Tel">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="form-control sa_validation"  name="email" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" class="form-control sa_validation" name="subject" placeholder="Subject">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea class="form-control" id="" rows="3" name="message" placeholder="Message"></textarea>
                            </div>
                        </div><!--row-->
                        
                            <input type="hidden" name="form_value" id="sa_form_value">
                            <input type="hidden" name="form_type" value="contact_form">
                            <input type="hidden" name="formdata_check" id="sa_formdata_check">
                        <button type="submit" value="" id="" class="btn btn-submit">Submit</button>
                    </form>	
                </div>

            </div>

        </div><!--row-->

    </div>
</section>

<section class="location-sec wow fadeIn">
    <div class="location">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15690.949046446092!2d76.2210798!3d10.5213482!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x629c039c3417144e!2sSmriti%20Design!5e0!3m2!1sen!2sin!4v1597383385009!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
</section>
<script type="text/javascript">

    (function ($, W, D)

    {
        var JQUERY4U = {};
        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        var current_form_name = "sa_contact_form";
                        var current_form = $("#" + current_form_name);

                        $("#sa_contact_form").validate({
                            rules: {
                                name: {
                                    required: true,
                                },
                                mobile: {
                                    required: true,
                                },
                                email: {

                                    required: true,
                                    email: true,
                                },
                                subject: {
                                    required: true,
                                }
                            },
                            messages: {

                            },
                            submitHandler: function (form) {

                                formdata();
                                formdata_check();

                                ajaxindicatorstart('please wait..');
                                $.ajax({

                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index/mailer",
                                    cache: false,
                                    data: new FormData(document.getElementById(current_form_name)),
                                    contentType: false,
                                    processData: false,
                                    success: function (msg) {
                                        ajaxindicatorstop();
                                        $(current_form)[0].reset();
                                        flash_message(msg);
                                    }

                                });
                            }

                        });
                    }

                };

        $(D).ready(function ($) {

            JQUERY4U.UTIL.setupFormValidation();
        });
    })(jQuery, window, document);


    function formdata() {

        var form_value = [];
        name = $('.sa_contact_form').find("input[name^='name']").val();
        email = $('.sa_contact_form').find("input[name^='email']").val();
        mobile = $('.sa_contact_form').find("input[name^='mobile']").val();
        subject = $('.sa_contact_form').find("input[name^='subject']").val();
        message = $('.sa_contact_form').find("textarea[name^='message']").val();

        form_value.push({name: name, email: email, mobile: mobile,subject: subject, message: message});
        document.getElementById("sa_form_value").value = JSON.stringify(form_value);

    }

    function formdata_check() {
        var form_names = [];
        $('.sa_formdata_check .sa_validation').each(function () {

            form_names.push($(this).attr('name'));
        });
        document.getElementById("sa_formdata_check").value = JSON.stringify(form_names);
    }

</script>