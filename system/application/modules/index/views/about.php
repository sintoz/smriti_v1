<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2>About Us</h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">About Us</li>
        </ol>
    </div>	
</section>

<section class="about-sec">
    <div class="container">

        <div class="row">

            <div class="col-sm-3">

                <div class="box">

                    <div class="iconbox wow fadeInUp">
                        <span class="icon-calendar iconstyle"></span>
                        <div class="title">Since<br>February 1997</div>
                    </div>

                    <div class="iconbox wow fadeInUp">
                        <span class="icon-projects iconstyle"></span>
                        <div class="title">More than<br>20, 000 Works</div>
                    </div>

                </div>

            </div>

            <div class="col-sm-9">
                <div class="desc-one wow fadeInUp">
                    Smriti design is started on17th February, 1997 under the leadership of Mr. David Thekkiniyath. Smriti is situated at Thrissur- Cultural capital of Kerala. We are the group of designers which are capable of promote your bussiness through logos, Packages, Brochures etc. We have successfully completed 20 years and around 20 thousand works.
                </div>
                <div class="desc">
                    <p class="wow fadeInUp">Graphic Design is an assortment of Creative Idea, Conceptualization, Skills and Technology. The process of designing a Graphic is begins with communication. The first step in Custom Graphic Design and Custom logo design is to understand the desired outcome. Whether you need a business card, company logo design, corporate identity design, corporate brochure design, business stationary design or magazine design.</p>

                    <p class="wow fadeInUp">Each should achieve the goal of marketing, corporate branding or selling your product. Being a graphics design firm & print design company we are providing an array of graphic design solutions, which includes logo design, symbol design, corporate identity design, stationary design, pre-press artwork design like brochures, catalogues, annual reports, 
                        magazines, flyer, poster and packaging design.</p>

                    <p class="wow fadeInUp">Graphic Design is an assortment of Creative Idea, Conceptualization, Skills and Technology. The process of designing a Graphic is begins with communication. The first step in Custom Graphic Design and Custom logo design is to understand the desired outcome. Whether you need a business card, company logo design, corporate identity design, corporate brochure design, business stationary design or magazine design.</p>
                </div>
            </div>

        </div><!--row-->

    </div>
</section>



<section class="history-sec">
    <div class="container">
        <h2>History of Smriti</h2>
        <div class="horizontal-timeline" id="example">
            <div class="events-content">
                <ol>
                    <li class="selected" data-horizontal-timeline='{"date": "1/01/1997"}'>
                        <div class="sub-title">Inauguration Invitation of Smriti design</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-1997.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/02/1997"}'>
                        <div class="sub-title">On February 17 Inauguration of Smriti design</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-1997a.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/1998"}'>
                        <div class="sub-title">The First Digital Design Studio in Thrissur</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-1998.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2001"}'>
                        <div class="sub-title">Best Souvenir Award from CHF, Mannuthy</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2001.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2006"}'>
                        <div class="sub-title">Best Designing Award from AICOG Kerala Conferance</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2006.jpg" alt=""/></div>
                    </li>
                    <li data-horizontal-timeline='{"date": "1/01/2009"}'>
                        <div class="sub-title">First Website Launched in 2009</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2009.jpg" alt=""/></div>
                    </li>


                    <li data-horizontal-timeline='{"date": "1/01/2010"}'>
                        <div class="sub-title">Best Designing Award from ISAR Kochi Conferance</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2010.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2013"}'>
                        <div class="sub-title">Best Designing Award from Ollur Forane Church</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2013.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2016"}'>
                        <div class="sub-title">Best Designing Award from Vela Committee MG Kavu Desam</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2016.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2017"}'>
                        <div class="sub-title">Smriti design Received Trademark Registration</p>
                            <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2017.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2019"}'>
                        <div class="sub-title">Centinary Logo Design award from TUCB</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2019.jpg" alt=""/></div>
                    </li>

                    <li data-horizontal-timeline='{"date": "1/01/2020"}'>
                        <div class="sub-title">First online Digital Design Studio in Kerala</div>
                        <div class="timeline-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/timeline-event-2020.jpg" alt=""/></div>
                    </li>

                </ol>
            </div>
        </div>	

    </div>
</section>
