
<section class="hm-sectorlist-sec">
    <div class="container">
        <h3 class="wow fadeInUp">We've created <span>2,00,000</span>+ incredible designs since 1997</h3>
        <div class="row">

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Arts & Culture</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/art1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/art.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/art1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Banking & Finance</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/bank1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/bank.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/bank1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Celebrations</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/celebration1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/celebration.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/celebration1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">	
                        <h4>Design & Architecture</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/design_arc1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/design_arc.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/design_arc1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Education</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/education1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/education.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/education1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Entertainment</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/entertainment1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/entertainment.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/entertainment1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Fashion & Beauty</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/beauty1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/beauty.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/beauty1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Food & Drink</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/food1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/food.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/food1.jpg'" alt=""/>
                        </div>
                    </a>	
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Health Care</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/health1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/health.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/health1.jpg'" alt=""/>
                        </div>
                    </a>	
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Hospitality & Leisure</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/hospital.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/hospital.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/hospital.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Manufacturing & Industrials</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/mfg1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/mfg.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/mfg1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>NGOs</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/ngo1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/ngo.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/ngo1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Professional Servies</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/prof1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/prof.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/prof1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Publishing</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/publi1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/publi.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/publi1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Real Estate</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/real1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/real.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/real1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Retail</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/retail1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/retail.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/retail1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Stationary</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/tech1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/tech.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/tech1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-4 col-sm-6">
                <div class="sector-item wow fadeInUp">
                    <a href="<?php echo base_url(); ?>portfolio">
                        <h4>Transport</h4>
                        <div class="item-image">
                            <img src="<?php echo base_url() . 'assets/web_end/' ?>images/trans1.jpg" onmouseover="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/trans.gif'" onmouseout="this.src = '<?php echo base_url() . 'assets/web_end/' ?>images/trans1.jpg'" alt=""/>
                        </div>
                    </a>
                </div>
            </div>

        </div>

        <div class="btn btn-gallery wow fadeIn"><a href="<?php echo base_url(); ?>portfolio">Browse Gallery</a></div>

    </div>
</section>