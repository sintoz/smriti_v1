<nav class="navbar navbar-expand-md">
    <a class="btn-filter collapsed" type="button" data-toggle="collapse" data-target="#filter" aria-expanded="false" aria-label="Toggle navigation">Filter by Category</a>
    <div class="collapse navbar-collapse hover-dropdown flex-column" id="filter">
        <div class="pro-filter wow fadeIn">

            <form>
                <div class="title">
                    <div class="row">
                        <div class="col-auto mr-auto">
                            <h6>Filter</h6>
                        </div>

                        <div class="col-auto">
                            <!--<button type="reset" value="" class="btn btn-txt">Reset</button>-->
                        </div>
                    </div><!--row-->
                </div>

                <div id="accordion">

                    <?php
                    if(!empty($_GET['service'])){
                     $service=$_GET['service'];
                    $service_array=explode("~",$service);   
                    }else{
                      $service_array=array();  
                    }
                    
                    if (!empty($services)) {
                        $i = 0;
                        foreach ($services as $skey => $service) {
                            $i++;
                            ?>
                            <div class="card">
                                <div class="card-header" id="<?php echo $i; ?>">
                                    <h5>
                                        <a class="d-block collapsed" data-toggle="collapse" href="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapse<?php echo $i; ?>">
                                            <i class="fa fa-angle-down"></i>
                                            <?php echo ucwords($service['name']); ?>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="<?php echo $i; ?>" data-parent="#accordion">
                                    <div class="card-body">
                                        <div class="pr-filt-<?php echo $i; ?>">
                                            <?php
                                            $conditional_array = array("media_type" => $skey);
                                            $extra_parameter_array = array();
                                            $main_services = $this->my_model->ResultData("ms_category", "id", "ASC", $conditional_array, $extra_parameter_array);
                                            if (!empty($main_services)) {
                                                foreach ($main_services as $main_key => $main_service) {
                                                    ?>

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" value="<?php echo $main_service->id; ?>" class="<?php if($main_service->parent_id==0){ echo " all ";}?>custom-control-input sa_filter sa_filter_category" id="<?php echo $main_service->id.$main_key; ?>" <?php if(in_array($main_service->id,$service_array)){ echo " checked ";}?>>
                                                        <label class="custom-control-label" for="<?php echo $main_service->id.$main_key; ?>"><?php echo ucwords($main_service->category); ?></label>
                                                    </div>

                                                    <?php
                                                }
                                            }
                                            ?>




                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>








                </div><!--accordion-->

            </form>
        </div><!--pro-filter-->
    </div>
</nav>