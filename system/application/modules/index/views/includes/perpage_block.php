<?php
if (!empty($data_list)) {
    foreach ($data_list as $data_key=>$data_row) {
        $media_file = json_decode($data_row->media_file, TRUE);
        $media_filename = $media_file[0]['filename'];
        $media_file_title = $media_file[0]['title'];
        $title = $data_row->title;
        $category_row = $this->my_model->RowData("ms_category", $data_row->category, "id");
        ?>
        <div class="col-lg-4 col-6">
            <div class="portfolio-box wow fadeInUp">
                <div class="portfolio-image">
                    <a href="javascript:void(0);" class="sa_media_popup" data-id="<?php echo $data_row->id;?>" data-caption="<?php echo $data_row->title;?>">
                        <img src="<?php echo base_url() . 'media_files/' . $media_filename; ?>" alt="<?php echo $media_file_title; ?>"  title="<?php echo $last_id; ?>"/>
                    </a>
                </div>
                <div class="portfolio-heading">
                    <h4><?php echo ucwords($title); ?></h4>
                    <h5><?php echo ucwords($category_row->category); ?></h5>
                </div>
            </div>
        </div>   
        <?php
         $last_id++;  
       
    }
    if ($perpage == count($data_list)) {
        ?>
        <div class="sa_ajax_load col-md-12" data-last_id="<?php echo $last_id; ?>" style="display: none;">
            More records found
        </div>
    <?php } else { ?>
        <div class="sa_ajax_load col-md-12" data-last_id="no_more">
            No more records found
        </div>
    <?php }  
    } ?>
