

<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h3><?php echo ucwords($data_row->title); ?></h3>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
            <?php
            if ($data_row->media_type == "5") {
                ?>

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="<?php echo $data_row->short_description; ?>" allowfullscreen=""></iframe>
                </div>

                <?php
            } else {
                $media_file = json_decode($data_row->media_file, TRUE);
                $media_filename = $media_file[0]['filename'];
                $media_file_title = $media_file[0]['title'];
                ?>
                <img src="<?php echo base_url() . 'media_files/' . $media_filename; ?>" alt="<?php echo $media_file_title; ?>"  title="<?php echo $data_row->id; ?>"/>
            <?php } ?>

        </div>
    </div>
</div>
