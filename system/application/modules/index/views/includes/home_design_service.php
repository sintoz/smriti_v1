<section class="hm-prof-serv-sec wow fadeInUp">
    <div class="container">

        <h4>Our Professional Services</h4>

        <div id="owl-demo" class="owl-carousel owl-theme">

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/package-design.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Package Design</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/3d-animation.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>3D Animation</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/book-cover-design.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Book Cover Design</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/logo-design.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Logo Design</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/social-media-design.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Social Media Design</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/data-entry.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Data Entry</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/static-web-design.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Static Web Design</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/e-commerce-web.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>E-Commerce Web</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/explain-animation.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Explain Animation</h5>
                        </div>
                    </a>
                </div>
            </div>

            <div class="item">
                <div class="image-box">
                    <a href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">
                        <img src="<?php echo base_url() . 'assets/web_end/' ?>images/whiteboard-animation.jpg" alt=""/>
                        <div class="overlay-box">
                            <h5>Whiteboard Animation</h5>
                        </div>
                    </a>
                </div>
            </div>

        </div>

    </div>
</section>
<?php /* ?>
  <section class="hm-prof-serv-sec wow fadeInUp">
  <div class="container">

  <h4>Our Professional Services</h4>

  <div id="owl-demo" class="owl-carousel owl-theme">
  <?php if(!empty($spl_services)){
  foreach($spl_services as $spl_service){
  $category_file=json_decode($spl_service->category_file,true);

  ?>
  <div class="item">
  <div class="image-box">
  <a href="<?php echo base_url().'services?service_id='.$spl_service->media_type;?>">
  <img src="<?php echo base_url().'media_files/'.$category_file[0]['filename']?>" alt="<?php echo $category_file[0]['filename'];?>"/>
  <h5><?php echo ucwords($spl_service->category);?></h5>
  </a>
  </div>
  </div>

  <?php   }

  }?>
  </div>

  </div>
  </section>
  <?php /* */ ?>