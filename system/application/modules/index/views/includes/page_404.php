<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
                <![endif]-->

<style type="text/css">


    * {
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    body {
        padding: 0;
        margin: 0;
        font-family: 'Montserrat', sans-serif;
        font-size: 16px;
        color: #666;
        background: rgb(255,255,255);
        background: linear-gradient(0deg, rgba(255,255,255,1) 25%, rgba(189,189,189,1) 100%);
    }

    #notfound {
        position: relative;
        height: 100vh;
    }

    #notfound .notfound {
        position: absolute;
        left: 50%;
        top: 50%;
        -webkit-transform: translate(-50%, -50%);
        -ms-transform: translate(-50%, -50%);
        transform: translate(-50%, -50%);
    }

    .notfound {
        max-width: 520px;
        width: 100%;
        text-align: center;
        line-height: 1.4;
        padding: 0 10px;
    }

    .notfound .notfound-404 {
        margin: 0 0 20px 0;
    }

    .notfound .notfound-404 h1 {
        font-size: 146px;
        font-weight: 700;
        margin: 0px;
        color: #232323;
    }

    .notfound .notfound-404 h1>span {
        display: inline-block;
        width: 120px;
        height: 120px;
        background-image: url('assets/developer_files/images/emoji.png');
        background-size: cover;
        -webkit-transform: scale(1.4);
        -ms-transform: scale(1.4);
        transform: scale(1.4);
        z-index: -1;
    }

    .notfound h2 {
        font-size: 50px;
        font-weight: 700;
        margin: 0 0 15px 0;
        text-transform: uppercase;
        color: #666;
    }

    .notfound p {
        font-weight: 500;
        font-size: 20px;
        margin: 0 0 30px 0;
    }

    .notfound a {
        display: inline-block;
        padding: 12px 30px;
        font-size: 14px;
        font-weight: 700;
        border:2px solid #d6d6d6;
        color: #666;
        border-radius: 40px;
        text-decoration: none;
        -webkit-transition: 0.2s all;
        transition: 0.2s all;
        margin: 0 5px;
    }

    .notfound a:hover {
        border:2px solid #666;
        background: #666;
        color: #fff;
    }

    @media only screen and (max-width: 767px) {
        .notfound .notfound-404 {
            margin: 0 0 10px 0;
        }
        .notfound .notfound-404 h1 {
            font-size: 86px;
        }
        .notfound .notfound-404 h1>span {
            width: 86px;
            height: 86px;
        }

        .notfound h2 {
            font-size: 36px;
            margin: 0 0 10px 0;
        }

        .notfound p {
            font-size: 14px;
            margin: 0 0 20px 0;
        }


        .notfound a {
            padding: 5px 20px;
            font-size: 12px;
            margin: 0 2px;
        }

    }


</style>

<div id="notfound">
    <div class="notfound">
        <div class="notfound-404">
            <h1>404</h1>
        </div>
        <h2>Sorry !</h2>
        <p>The page you are looking for does not found</p>

        <a href="javascript:void(0)" onClick="goBack()">Go Back</a>
        <a href="<?php echo base_url(); ?>">Home</a>


    </div>
</div>
<script type="text/javascript">
    function goBack() {
        window.history.back();
    }
</script> 	

