<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2><?php echo ucwords($services[$service_id]['name']);?></h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
            <li class="breadcrumb-item active">Services</li>
        </ol>
    </div>	
</section>

<section class="catg-sec">
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <div class="catg-menu wow fadeIn">
                     <?php
                                if (!empty($services)) {
                                    $i = 1;
                                    $service_id ="";
                                    if(!empty($_GET['service_id'])){
                                      $service_id =$_GET['service_id'];
                                    }
                                    foreach ($services as $serkey => $service) {
                                        ?>
                    <a class="catg-menu-item <?php if($service_id==$serkey){ echo " active ";}?>" href="<?php echo base_url() . 'services?service_id=' . $serkey; ?>"><?php echo ucwords($service['name']); ?></a>
                      <?php
                                        $i++;
                                    }
                                }
                                ?>
                </div>
            </div>

            <div class="col-md-9">

                <div class="catg-row">

                    <div class="row">
                        <?php
                        if (!empty($service_list)) {
                            foreach ($service_list as $skey => $service_row) {

                                $service_id=$service_row->id;
                                $service_category=$service_row->category;
                                $service_identity=$service_row->identity;
                                $service_file=$service_row->category_file;
                                $service_file=json_decode($service_file,TRUE);
                                $filename=$service_file[0]['filename'];
                                $file_title=$service_file[0]['title'];
                                $service_description=$service_row->description;
//                                $service_price=number_format($service_row->extra_1,2);
                                $service_price=$service_row->extra_1;
                                
                                ?>
                                <div class="col-lg-4 col-sm-6">
                                    <div class="catg-item wow fadeInUp">
                                        <a href="<?php echo base_url().'service/'.$service_identity.'?service_id='.$service_id ?>">
                                            <div class="catg-image"><img src="<?php echo base_url()."media_files/".$filename;?>" alt="<?php echo $file_title;?>"/></div>
                                            <div class="catg-content-box">
                                                <div class="catg-txt">
                                                    <div class="catg-name"><?php echo $service_category;?></div>
                                                    <div class="catg-price">from &#8377; <?php echo $service_price;?></div>
                                                </div>
                                                <div><a class="btn btn-get" href="<?php echo base_url().'service/'.$service_identity.'?service_id='.$service_id; ?>">Get Started</a></div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        ?>
       


                    </div><!--row-->

                </div><!--catg-row-->



            </div>

        </div><!--row-->

    </div>
</section>