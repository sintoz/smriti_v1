<?php
$service_id=$service_row->id;
$service_category=$service_row->category;
$service_media_type=$service_row->media_type;
$service_file=$service_row->category_file;
$service_file=json_decode($service_file,TRUE);
$filename=$service_file[0]['filename'];
$file_title=$service_file[0]['title'];
$service_description=$service_row->description;
$service_price=$service_row->extra_1;
//$service_price=number_format($service_row->extra_1,2);

$service_type=$services[$service_media_type]['name'];
?>
<section class="sub-banner-sec wow fadeIn">
<div class="container">
<h2><?php echo ucwords($service_category);?></h2>
</div>
</section>
<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url();?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo base_url().'services?service_id='.$service_media_type?>"><?php echo ucwords($service_type);?></a></li>
            <li class="breadcrumb-item active"><?php echo ucwords($service_category);?></li>
        </ol>
    </div>	
</section>
<section class="serv-detail-sec">
    <div class="container">

        <div class="row">

            <div class="col-lg-7">
                <div class="serv-detail-row">
                    <h3 class="wow fadeInUp"><?php echo ucwords($service_category);?></h3>

                    <div class="image-box wow fadeInUp"><img src="<?php echo base_url()."media_files/".$filename;?>" alt="<?php echo $file_title;?>"/></div>

                  <?php echo $service_description;?>

                    <div class="price-box wow fadeIn">
                        <div class="price">
                            <div class="txt-one">Price</div>
                            <div class="txt-two">from &#8377; <?php echo $service_price;?></div>
                        </div>
                        <div><a class="btn btn-view-portfolio" href="<?php echo base_url().'portfolio?service=~'.$service_id.'~'?>">Go to Portfolio</a></div>
                    </div>

                </div>
            </div>
            <div class="col-lg-5">
                <div class="get-quote wow fadeIn">
                    <h3>Get a Quote</h3>
                    <div class="desc">Please complete the form to receive a free Quotation for our services. We will reply as soon as possible.</div>
                    <div class="contact-form">
                        <form id="sa_quote_form" class="sa_quote_form sa_formdata_check" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control sa_validation" name="name" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control sa_validation"  name="mobile" placeholder="Mobile">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control sa_validation"  name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <textarea class="form-control sa_validation"  name="requiremnt" rows="7" placeholder="Requirement"></textarea>
                            </div>
                            <input type="hidden" name="service_name" value="<?php echo $service_category;?>">
                            <input type="hidden" name="category_id" value="<?php echo $service_id; ?>">
                            <input type="hidden" name="media_type" value="<?php echo $service_media_type; ?>">
                            <input type="hidden" name="form_value" id="sa_form_value">
                            <input type="hidden" name="form_type" value="quote_form">
                            <input type="hidden" name="formdata_check" id="sa_formdata_check">
                            <button type="submit" class="btn btn-submit btn-block">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

        </div><!--row-->

    </div>
</section>


<section class="similar-items-sec">
    <div class="container">

        <h3>Similar <?php echo ucwords($service_type);?> Services</h3>

        <div id="owl-similat-items" class="owl-carousel owl-theme wow fadeIn">
            <?php
            if (!empty($service_list)) {
                foreach ($service_list as $skey => $service_row) {
                    
                    $service_id = $service_row->id;
                    $service_category = $service_row->category;
                    $service_identity = $service_row->identity;
                    $service_file = $service_row->category_file;
                    $service_file = json_decode($service_file, TRUE);
                    $filename = $service_file[0]['filename'];
                    $file_title = $service_file[0]['title'];
                    $service_description = $service_row->description;
                    $service_price = number_format($service_row->extra_1, 2);
                    ?>
                    <div class="item">
                        <div class="pro-item">
                            <a href="<?php echo base_url().'service/'.$service_identity.'?service_id='.$service_id ?>">
                                <div class="image"><img src="<?php echo base_url()."media_files/".$filename;?>" alt="<?php echo $file_title;?>" /></div>
                                <h4><?php echo ucwords($service_category);?></h4>
                            </a>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>

        </div>

    </div>
</section>
<script type="text/javascript">

    (function ($, W, D)

    {
        var JQUERY4U = {};
        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        var current_form_name = "sa_quote_form";
                        var current_form = $("#" + current_form_name);

                        $("#sa_quote_form").validate({
                            rules: {
                                name: {
                                    required: true,
                                },
                                mobile: {
                                    required: true,
                                },
                                email: {

                                    required: true,
                                    email: true,
                                },
                                requiremnt: {
                                    required: true,
                                }
                            },
                            messages: {

                            },
                            submitHandler: function (form) {

                                formdata();
                                formdata_check();

                                ajaxindicatorstart('please wait..');
                                $.ajax({

                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index/mailer",
                                    cache: false,
                                    data: new FormData(document.getElementById(current_form_name)),
                                    contentType: false,
                                    processData: false,
                                    success: function (msg) {
                                        ajaxindicatorstop();
                                        $(current_form)[0].reset();
                                        flash_message(msg);
                                    }

                                });
                            }

                        });
                    }

                };

        $(D).ready(function ($) {

            JQUERY4U.UTIL.setupFormValidation();
        });
    })(jQuery, window, document);


    function formdata() {

        var form_value = [];
        name = $('.sa_quote_form').find("input[name^='name']").val();
        email = $('.sa_quote_form').find("input[name^='email']").val();
        service_name = $('.sa_quote_form').find("input[name^='service_name']").val();
        mobile = $('.sa_quote_form').find("input[name^='mobile']").val();
        requiremnt = $('.sa_quote_form').find("textarea[name^='requiremnt']").val();

        form_value.push({name: name, email: email, mobile: mobile,service: service_name, requiremnt: requiremnt});
        document.getElementById("sa_form_value").value = JSON.stringify(form_value);

    }

    function formdata_check() {
        var form_names = [];
        $('.sa_formdata_check .sa_validation').each(function () {

            form_names.push($(this).attr('name'));
        });
        document.getElementById("sa_formdata_check").value = JSON.stringify(form_names);
    }

</script>