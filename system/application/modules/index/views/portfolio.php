<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2>Portfolio</h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Portfolio</li>
        </ol>
    </div>	
</section>

<section class="portfolio-sec">
    <div class="container">

        <div class="row">

            <div class="col-lg-3 col-md-4">
                <div class="filter-menu">
                    <?php $this->load->view('includes/left_filter'); ?>
                </div>
            </div>

            <div class="col-lg-9 col-md-8">

                <div class="portfolio-row">

                    <div class="row sa_result_block">
                        <?php $this->load->view('includes/perpage_block'); ?>

                    </div><!--row-->
                    <!--<div class="sa_removethis" style="height:100px; float: left; width:100%"></div>-->

                </div><!--portfolio-row-->

            </div>

        </div><!--row-->

    </div>
</section>
<div class="modal fade sa_pop_data"></div>
<script>
    // $(document).ready(function () {

    //     $('body').on('touchmove', onScroll);
    //     $(window).on('scroll',onScroll );
    // });

//    function onScroll() {
//        var last_id = $('.sa_ajax_load').attr('data-last_id');
//
//        if (($(window).scrollTop() == $(document).height() - $(window).height() )&& (last_id != 'no_more')) {
// 
//            var hashes = hashvaluepass();
//            var type = "scroll";
//            loadMoreData(last_id, hashes, type);
//        }
//    }



    function loadMoreData(last_id, hashes, type) {
        ajaxindicatorstart('please wait..');
        var base_url = $(".base_url").val();
        $.ajax({
            type: 'POST',
            url: base_url + "index/load_more_data",
            data: {last_id: last_id, hashes: hashes},
            beforeSend: function () {
                $('.sa_ajax_load').show();
            },
            success: function (response) {
                $('.sa_ajax_load').remove();
                if (type == "filter") {
                    $(".sa_result_block").html(response).delay("slow").fadeIn();
                } else if (type == "scroll") {
                    $(".sa_result_block").append(response).delay("slow").fadeIn();
                }
                ajaxindicatorstop();
            }
        });
    }


    $("body").on("change", ".sa_filter_category", function () {
        var parameter = "~";
        var i = 0;
        $(".sa_filter:checked").each(function () {
            i++;
            var parameter_item = $(this).val();
            parameter += parameter_item + "~";
        });

        if (i > 0) {
            var param_set = "service=" + parameter;
            var url = window.location.href;
            var url_splite = url.split("?");
            var new_url = url_splite[0] + "?" + param_set;
            history.pushState('', '', new_url);
            var hashes = hashvaluepass();
            var last_id = '0';
            var type = "filter";
            loadMoreData(last_id, hashes, type);
        } else {
            var url = window.location.href;
            var url_splite = url.split("?");
            var new_url = url_splite[0] + "?";
            history.pushState('', '', new_url);
            var hashes = hashvaluepass();
            var last_id = '0';
            var type = "filter";
            loadMoreData(last_id, hashes, type);
        }

    });
    $(document).ready(function () {
        var hashes = hashvaluepass();
        if (hashes == "") {
            hashes = "";
        }
        var last_id = '0';
        var type = "filter";
        loadMoreData(last_id, hashes, type);
    });
    function hashvaluepass() {
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        return hashes;
    }
    
    
    $("body").on("click",".sa_media_popup",function(){
        var base_url = $(".base_url").val();
        var id = $(this).attr("data-id");
        $.ajax({
            type: 'POST',
            url: base_url + "index/load_unique_data",
            data: {id: id},
            success: function (response) {
                $(".sa_pop_data").html(response);
                $(".sa_pop_data").modal('show');
                
            }
        }); 
    });

</script>