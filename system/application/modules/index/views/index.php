<section class="hm-services-sec">
    <div class="container">

        <h1 class="wow fadeInUp">Take Your Business<br><span>Online Today</span></h1>

        <div class="row">
            <?php
            if (!empty($services)) {
                $i = 1;
                foreach ($services as $serkey => $service) {
                    ?>
                    <div class="col-md-4">
                        <div class="service-box wow fadeInUp" data-wow-delay="0.<?php echo $i; ?>s">
                            <div class="icon">

                                <div class="hi-icon-wrap hi-icon-effect-5 hi-icon-effect-5c">
                                    <a href="<?php echo base_url() . 'services?service_id=' . $serkey; ?>" class="hi-icon <?php echo $service['home_icon']; ?>"></a>
                                </div>

                            </div>
                            <h2><?php echo ucwords($service['name']); ?></h2>
                            <a class="btn btn-more" href="<?php echo base_url() . 'services?service_id=' . $serkey; ?>">Get Started</a>
                        </div>
                    </div>


                    <?php
                    $i++;
                }
            }
            ?>


        </div>
    </div>
</section>

<?php $this->load->view('includes/home_design_service'); ?>


<section class="hm-process-sec wow fadeIn">
    <div class="container">

        <div class="row">

            <div class="col-xl-6">

                <h2 class="wow fadeInUp">How Smriti Works ?</h2>

                <div class="process-row">

                    <div id="owl-process" class="owl-carousel owl-theme">

                        <div class="item">
                            <div class="process-box">
                                <div class="number">1</div>
                                <div class="title">Step 1</div>
                                <div class="desc">Visit : smritidesign.com website on your Laptop or Mobile.</div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">2</div>
                                <div class="title">Step 2</div>
                                <div class="desc">Select the design template  that suit your needs.</div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">3</div>
                                <div class="title">Step 3</div>
                                <div class="desc">Highlight your needs.</div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">4</div>
                                <div class="title">Step 4</div>
                                <div class="desc"><p>We will contact you by phone or email within 24 hours.</p><p>Identify the basic requistes you need in your layout.</p></div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">5</div>
                                <div class="title">Step 5</div>
                                <div class="desc">We will despatch  you the files designed by our experts according to your description.</div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">6</div>
                                <div class="title">Step 6</div>
                                <div class="desc">Editing facilities will be done by phone or through  email.</div>
                                <div class="arrow_1"><img src="<?php echo base_url() ?>assets/web_end/images/arrow_1.png" alt=""/></div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="process-box">
                                <div class="number">7</div>
                                <div class="title">Step 7</div>
                                <div class="desc">On remittance of bills   through Bank ( Through Online remittance to the Bank)  you can avail the receipt as well the final file of your print matter.</div>
                            </div>
                        </div>

                    </div>

                    <a class="btn btn-more light wow fadeInUp" href="<?php echo base_url() . 'services?service_id=' . $design_serive; ?>">Get Started</a>

                </div>

            </div>

            <div class="col-xl-6">
                <div class="video wow fadeIn">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VOHTSXmoLXE" allowfullscreen></iframe>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>




<?php $this->load->view('includes/home_design_category'); ?>




<section class="hm-testimonials-sec wow fadeIn">
    <div class="container">

        <h3 class="wow fadeInUp">Here's what real business say about Smriti Design</h3>

        <div class="testimonial-box wow fadeIn">
            <div id="owl-testimonials" class="owl-carousel owl-theme">


                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "Smriti Design changes the game for getting creative work done. My personal opinion? Its a great service and I highly recommend it."
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Binni Emmatty</div>
                                    <div class="sub-title">
                                        President<br>
                                        All Kerala Vyapari Vyavasai Samithy<br>
                                        2014
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/binni.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>



                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "The speed and efficiency in receiving my designs was very impressive. Many thanks for your quick respones to my queries. Much appreciated."
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Dr. Fessy Louis T</div>
                                    <div class="sub-title">
                                        Vice President,<br>
                                        Federation of Obstetric and<br>
                                        Gynaecological Societies of India
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/fessy.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>



                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "Smriti Design is a platform with a very good service. I would definitely recommend Smriti to anyone looking for logos and branding."
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Dr. Paul Poovathingal</div>
                                    <div class="sub-title">
                                        Director<br>
                                        Chetana Group of Institutions<br>
                                        Thrissur
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/paul.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>



                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "With Smriti, we are able to find Excellent design, they also encourages us to design in various styles"
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Fr. Jose Thanickal</div>
                                    <div class="sub-title">
                                        Principal<br>
                                        Carmel Higher Secondary School<br>
                                        Chalakudy<br>
                                        2016
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/jose-thanickal.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>


                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "Smriti design is a brilliant way to pick the creative designs."
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Fr. Viju. Kolamkanny CMI</div>
                                    <div class="sub-title">
                                        Administrator<br> 
                                        Christ Academy Educational Institutions<br>
                                        Bangalore
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/viju.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>


                <div class="item wow fadeIn">
                    <div class="testimonial">

                        <div class="desc-box">

                            <div class="desc">
                                "Great experience with Smriti Design. Designers offered design based exactly on what I requested and fresh ideas as well. This is an awesome service."
                            </div>

                            <div class="testimonial-by">
                                <div class="txt-box">
                                    <div class="name">Dr. Venugopal</div>
                                    <div class="sub-title">
                                        Secretary<br>
                                        Kerala Federation of<br>
                                        Obstetrics & Gynaecology
                                    </div>
                                </div>
                                <div class="image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/venu.jpg" alt=""/></div>
                            </div>

                            <div class="icon-quote-one"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-left.png" alt=""/></div>
                            <div class="icon-quote-two"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/icon-quote-right.png" alt=""/></div>
                        </div>

                    </div>
                </div>



            </div>
        </div><!--testimonial-box-->

    </div>
</section>


<section class="hm-clients-sec wow fadeIn">
    <div class="container-fluid">

        <div id="owl-clients" class="owl-carousel owl-theme wow fadeIn">

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-1.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-2.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-3.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-4.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-5.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-6.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-7.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-8.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-9.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-10.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-11.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-12.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-13.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-14.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-15.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-16.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-17.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-18.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-19.jpg" alt=""/></div>
            </div>

            <div class="item">
                <div class="client-item"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/clients-20.jpg" alt=""/></div>
            </div>


        </div>

    </div>
</section>