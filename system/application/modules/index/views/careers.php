<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2>Careers</h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Careers</li>
        </ol>
    </div>	
</section>


<section class="careers-sec">
    <div class="container">

        <h3 class="wow fadeInUp">Start Your Career with Us</h3>

        <h5 class="wow fadeInUp">Come join us and make a difference</h5>

        <div class="desc wow fadeInUp">

            <p>We offer advertising careers with full-time schedules, competitive salaries and a stimulating work environment that fosters creativity and teamwork.</p>

            <p>Do you consider yourself a conjurer of conjugation? Become a Copywriter. Are you the Da Vinci of design? Become a Graphic Designer.
            </p>

            <p>Whichever position in advertising you choose to pursue, do it at Smriti Design</p>

        </div>

    </div>
</section>



<section class="careers-sec-two wow fadeIn">
    <div class="container">

        <div class="box-accordion wow fadeIn">
            <div id="accordion">

                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a class="collapsed d-block" data-toggle="collapse" href="#collapse1">
                                <i class="fa fa-angle-up"></i>Graphic Designer</a>
                        </h3>
                    </div>
                    <div id="collapse1" class="collapse" data-parent="#accordion">
                        <div class="card-body">

                            <h4>Job Requirements</h4>
                            <div class="desc">
                                <p>Professional knowledge in Typography, Layout and Pagesetting with experience in Adobe Photoshop, Coral Draw / Illustrator and Indesign.</p>

                                <p>Sketching and drawing skills are encouraged.</p>
                            </div>

                            <h4>Job Description</h4>

                            <ul class="list-item">
                                <li>Must have the ability to take design work yourself and do it responsibly on time.</li>
                                <li>Managing time and completing projects creatively and quickly and with minimal to no errors.</li>
                                <li>Working with fellow designers, the art director, and the creative director on concepts and ideas.</li>
                                <li>Ability to receive constructive criticism and provide feedback to the team.</li>
                            </ul>	

                        </div>
                    </div>
                </div>


                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a class="collapsed d-block" data-toggle="collapse" href="#collapse2">
                                <i class="fa fa-angle-up"></i>Animator</a>
                        </h3>
                    </div>
                    <div id="collapse2" class="collapse" data-parent="#accordion">
                        <div class="card-body">

                            <h4>Job Requirements</h4>
                            <div class="desc">
                                <p>As a Animater in Visual media you'll be working creatively everyday with Smriti teams, producing motion graphics, infographics & screen graphics using Adobe CC and a variety of other software.</p>

                                <p>Professional design experience in Adobe After effect, and Knowledge of Cinema 4D/3ds Max and/or other 3D software;  additional knowledge in other Adobe suite programs is a plus.</p>

                                <p>Sketching and drawing skills encouraged.</p>
                            </div>

                            <h4>Job Description</h4>

                            <ul class="list-item">
                                <li>Must have the ability to take Animation work yourself and do it responsibly on time.</li>
                                <li>Managing time and completing projects creatively and quickly and with minimal to no errors.</li>
                                <li>Working with fellow designers, the art director, and the creative director on concepts and ideas.</li>
                                <li>Ability to receive constructive criticism and provide feedback to the team.</li>
                            </ul>	

                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a class="collapsed d-block" data-toggle="collapse" href="#collapse3">
                                <i class="fa fa-angle-up"></i>Web Designer</a>
                        </h3>
                    </div>
                    <div id="collapse3" class="collapse" data-parent="#accordion">
                        <div class="card-body">

                            <h4>Job Requirements</h4>
                            <div class="desc">
                                <p>Design and develop the layout and the overall look of a website. Should be able to create graphic and media essentials using Adobe XD, Adobe Photoshop/Illustrator by considering colour and typography.</p>

                                <p>Very good knowledge in HTML5, CSS3, Bootstrap. Knowledge in Php, JS and other Scripting languages will be an added advantage.</p>

                                <p>Exposure to all types of social media campaigns.</p>
                            </div>

                            <h4>Job Description</h4>

                            <ul class="list-item">
                                <li>Must have the ability to take website work yourself and do it responsibly on time.</li>
                                <li>Optimising sites for maximum speed and scalability.</li>
                                <li>Maintain and update websites</li>
                            </ul>	

                        </div>
                    </div>
                </div>


            </div><!--accordion-->
        </div><!--box-accordion-->


    </div>
</section>




<section class="careers-form-sec wow fadeIn">
    <div class="container">

        <h3>Let's start now</h3>


        <div class="contact-form m-0">
            <form id="sa_multiple_upload_form" class="sa_contact_form sa_formdata_check sa_multiple_upload_form" enctype='multipart/form-data'>
                <div class="form-row">


                    <div class="form-group col-md-6">
                        <select class="form-control sa_validation" id="jobtitle"  name="jobtitle">
                            <option value="">Applying for</option>
                            <option value="Graphic Designer">Graphic Designer</option>
                            <option value="Animator">Animator</option>
                            <option value="Web Designer">Web Designer</option>
                            <option value="Interns Wanted">Interns Wanted</option>
                        </select>
                    </div>


                    <div class="form-group col-md-6">
                        <input type="text" name="specialized" class="form-control sa_validation"   placeholder="Specialized In">
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text" name="name" class="form-control sa_validation"   placeholder="Your Name">
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text" name="mobile" class="form-control sa_validation"   placeholder="Mobile">
                    </div>

                    <div class="form-group col-md-6">
                        <input type="email" name="email" class="form-control sa_validation"   placeholder="Email Id">
                    </div>

                    <div class="form-group col-md-6">
                        <input type="text" name="expertise" class="form-control"   placeholder="Social (Any blogs, dot com, Twitter, online presence?)">
                    </div>


                    <div class="form-group col-md-6">
                        <!--<label for="">Upload your Resume</label>-->
                        <?php
                        $fileuploadnames = $this->my_model->fileuploadnames();
                        $attachment_data['label'] = "Upload your Resume"; // file label
                        $attachment_data['name'] = "upload_image"; // file input name
                        $attachment_data['post_name'] = "upload_image_1"; // attachment save field [post data]
                        $attachment_data['count'] = "";  // multiple upload or single upload
                        $attachment_data['attachment_count'] = "career1"; // form attachment count
                        $attachment_data['image_type'] = "media_item_image"; // image type
                        $attachment_data['form_type'] = "web_attach_form"; // form type
                        $attachment_data['upload'] = $this->my_model->upload_rules(); // upload rules array

                        $this->load->view('attachment_view/attachment', $attachment_data);
                        ?>
                        <!--<input type="file" name=""   class="form-control-file sa_file_upload"/>-->
                    </div>

                </div><!--row-->
                <input type="hidden" name="form_value" id="sa_form_value">
                <input type="hidden" name="form_type" value="career_form">
                <input type="hidden" name="formdata_check" id="sa_formdata_check">
                <button type="submit" class="btn btn-submit">Submit</button>

            </form>
        </div>	
    </div><!--container-->
</section>
<script type="text/javascript">

    (function ($, W, D)

    {
        var JQUERY4U = {};
        JQUERY4U.UTIL =
                {
                    setupFormValidation: function ()
                    {
                        var current_form_name = "sa_multiple_upload_form";
                        var current_form = $("#" + current_form_name);

                        $("#sa_multiple_upload_form").validate({
                            rules: {
                                name: {
                                    required: true,
                                },
                                mobile: {
                                    required: true,
                                },
                                email: {

                                    required: true,
                                    email: true,
                                },
                                jobtitle: {
                                    required: true,
                                },
                                specialized: {
                                    required: true,
                                }
                            },
                            messages: {

                            },
                            submitHandler: function (form) {

                                formdata();
                                formdata_check();

                                ajaxindicatorstart('please wait..');
                                $.ajax({

                                    type: "POST",
                                    url: "<?php echo base_url(); ?>index/mailer",
                                    cache: false,
                                    data: new FormData(document.getElementById(current_form_name)),
                                    contentType: false,
                                    processData: false,
                                    success: function (msg) {
                                        ajaxindicatorstop();
                                        $(current_form)[0].reset();
                                        flash_message(msg);
                                        setTimeout(function () {
                                            location.reload();
                                        }, 1000);

                                    }

                                });
                            }

                        });
                    }

                };

        $(D).ready(function ($) {

            JQUERY4U.UTIL.setupFormValidation();
        });
    })(jQuery, window, document);


    function formdata() {

        var form_value = [];
        jobtitle = $('.sa_contact_form').find("select[name^='jobtitle'] option:selected").val();
        specialized = $('.sa_contact_form').find("input[name^='specialized']").val();
        name = $('.sa_contact_form').find("input[name^='name']").val();
        email = $('.sa_contact_form').find("input[name^='email']").val();
        mobile = $('.sa_contact_form').find("input[name^='mobile']").val();
        expertise = $('.sa_contact_form').find("input[name^='expertise']").val();
        attachment = $('.sa_image_remove').attr("data-filename");


        var full_url = "<a href='<?php echo base_url(); ?>media_files/" + attachment + "' download='" + name.replace(/\s/g, '') + "_cv'>Download File</a>";
        form_value.push({jobtitle: jobtitle, specialized: specialized, name: name, email: email, mobile: mobile, expertise: expertise, attachment: full_url});
        document.getElementById("sa_form_value").value = JSON.stringify(form_value);

    }

    function formdata_check() {
        var form_names = [];
        $('.sa_formdata_check .sa_validation').each(function () {

            form_names.push($(this).attr('name'));
        });
        document.getElementById("sa_formdata_check").value = JSON.stringify(form_names);
    }

</script>