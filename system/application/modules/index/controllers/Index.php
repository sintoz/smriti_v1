<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author SMS
 */
class Index extends MY_Controller {

    function __construct() {
        parent::__construct();
        
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('index_model');
        $this->load->library('ion_auth');
        $this->load->library('encryption');  
        
        
    }

    
    function index() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['design_serive']=4;
        $data['services']=$services=$this->my_model->media_types();
        $conditional_array=array("media_type"=>"4","parent_id !="=>"0");
        $extra_parameter_array=array("limit"=>"10");
        $data['spl_services']=$services=$this->my_model->ResultData("ms_category", "order_no", "DESC", $conditional_array,$extra_parameter_array);
        $this->template->load('master', 'index',$data);
    }
    function about() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'about',$data);
    }
    function services() {
        if(!empty($_GET['service_id'])){
          $data['service_id']= $service_id= $_GET['service_id'];
        }else{
            redirect('index');
        }
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $conditional_array=array("media_type"=>$service_id,"parent_id !="=>"0");
        $extra_parameter_array=array();
        $data['service_list']=$services=$this->my_model->ResultData("ms_category", "order_no", "DESC", $conditional_array,$extra_parameter_array);
        $this->template->load('master', 'services',$data);
    }
    
    
    function portfolio() {
        
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();  
        $this->template->load('master', 'portfolio',$data);
  }
  
  function load_more_data() {
        $data['perpage']=$perpage = "";
        $data['last_id']=$last_id=$this->input->post('last_id');
        $data['data_list'] = $data_list = $this->index_model->load_more_data($perpage);
        echo $this->load->view('includes/perpage_block', $data, TRUE);
    }
    
    function load_unique_data(){
       $id = $this->input->post('id');
       $data['data_row'] = $data_row = $this->my_model->RowData("ms_medias",$id,'id');
       echo $this->load->view('includes/popup_data', $data, TRUE); 
    }

    function contact() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'contact',$data);
    }
    function careers() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'careers',$data);
    }
    function privacy_poilcy() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'privacy_poilcy',$data);
    }
    function terms_and_conditions() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'terms_and_conditions',$data);
    }
    function payment_policy() {
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $this->template->load('master', 'payment_policy',$data);
    }

    function service_detail(){
        if(!empty($_GET['service_id'])){
            
           $service_id= $_GET['service_id'];
           
        }else{
            redirect('index');
        }
        $data['page']=$this->uri->segment(1);
        $data['page_meta_title']="Smriti Design";
        $data['page_meta_description']="";
        $data['page_meta_keywords']="";
        $data['services']=$services=$this->my_model->media_types();
        $data['service_row']= $service_row=$this->my_model->RowData("ms_category", $service_id, "id");
        
        $conditional_array=array("media_type"=>$service_row->media_type,"id !="=>$service_row->id,"parent_id !="=>"0");
        $extra_parameter_array=array("limit"=>"10");
        $data['service_list']=$services=$this->my_model->ResultData("ms_category", "order_no", "DESC", $conditional_array,$extra_parameter_array);
        $this->template->load('master', 'services_detail',$data); 
    }
    
    
    
    
    
    function mailer(){
        $form_names = json_decode($this->input->post('formdata_check'), TRUE);
        foreach ($form_names as $name) {
            $this->form_validation->set_rules($name, $name, 'required');
        }
        
        if ($this->form_validation->run() == FALSE) {

            echo validation_errors();
        } else {
            
            $data['formdata'] = $formdata = json_decode($this->input->post('form_value'), TRUE);
            $data['json_formdata']=$json_formdata = json_decode($this->input->post('form_value'), TRUE);
            $data['form_type'] = $form_type = $this->input->post('form_type');
            
            $settings_row=$this->my_model->settings();
            
            $data['to_mail'] = $to_mail=$settings_row->from_email;
            $data['from_mail'] = $from_mail=$settings_row->from_email;
            $project_name=project_name;


            $subj = str_replace('_', ' ', ucfirst($form_type)) . " from " . $project_name;

            $email = $formdata[0]['email'];
            
            if (isset($formdata[0]['name'])) {

               $data['name']= $name = $formdata[0]['name'];
            } else {

               $data['name']= $name = '';
            }
            
            $message = $this->load->view('mail/mailview', $data, true);

            if($form_type=="quote_form"){
                $this->index_model->order_creation();
            }
            
            
            $this->load->library('email');
            $this->email->from($from_mail, $name);
            $this->email->to($to_mail, $project_name);
            $this->email->reply_to($email, $name);
            $this->email->subject($subj);
            $this->email->message($message);

            if ($this->email->send()) {
                
                echo "Thank you - We will contact you soon..";
            } else {

                echo 'Mail not send';
            }   
            
        }
    }
    
    
    
    function page_not_found(){
      $data['page_meta_title']=project_name." | 404";
      $data['page_meta_description']="";
      $data['page_meta_keywords']="";  
      $data['services']=$services=$this->my_model->media_types();
      $this->template->load('master', 'includes/page_404',$data);  
    }
    
        
    function upload_file() {
        $input_name = $this->input->post('file_input_name');
        if (isset($_FILES[$input_name]['name'])) {
            $filename = $input_name; //$filename=file type name
           
            $this->my_model->upload_data($filename);
        }
    }
    
    function remove_upload_file() {
        $this->my_model->remove_upload_file();
    }

    
    
    
}
