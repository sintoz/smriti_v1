<?php
class Panel_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
    }



  function update_password() {


        $msg = $this->input->post('new');
        $msg= $this->security->xss_clean($msg);
        $encrypted_string = $this->encryption->encrypt($msg);

        $data = array(
            'password' => $encrypted_string,
        );

        $check_password = $this->input->post('old');
        $check_password= $this->security->xss_clean($check_password);
        $username = $this->session->userdata('logged_username');

        $this->db->where('username', $username);


        $this->db->update('admin', $data);

        if ($this->db->affected_rows() == '1') {
            return TRUE;
        }

        return FALSE;
    }

    function getpassword() {

        $check_password = $this->input->post('old');
        $check_password= $this->security->xss_clean($check_password);
        
        $username = $this->session->userdata('logged_username');

        $this->db->where('username', $username);
        $admin_details = $this->db->get('admin')->row();

        $msg1 = $admin_details->password;
        $encrypted_string1 = $this->encryption->decrypt($msg1);
        if ($encrypted_string1 == $check_password) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
    
    function save_email(){
     $frm_email= $this->security->xss_clean($this->input->post('frm_email'));   
     $to_email= $this->security->xss_clean($this->input->post('to_email'));
     $data=array(
         "from_email"=>$frm_email,
         "to_email"=>$to_email
         );
     
     $this->db->where('id', 1);
     $this->db->update('ms_settings', $data);
    }
}