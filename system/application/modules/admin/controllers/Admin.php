<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author sms
 */
class Admin extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('panel_model');
        $this->load->library('ion_auth');
        $this->load->library('encryption');


        if ($this->session->userdata('logged_adminpanel') != 'true') {
            redirect('stablemode/index');
        }
    }

    public function index() {
        if ($this->session->userdata('logged_adminpanel') == 'true') {

            $log_usernames = $this->session->userdata('logged_username');
            $this->db->where('username', $log_usernames);
            $loged_details = $this->db->get('admin')->row();
            $loged_type = $loged_details->type;

            if ($loged_details->type == 'super') {

                redirect('admin/home');
            }
        } else {
            $this->session->set_userdata('logged_adminpanel', 'false');
            $this->session->set_userdata('logged_username', '');
            redirect('stablemode/index');
        }
    }

    function logout() {
        $this->session->set_userdata('logged_adminpanel', 'false');
        redirect('admin/index');
    }

    public function home() {

        if ($this->session->userdata('logged_adminpanel') == 'true') {

            $log_usernames = $this->session->userdata('logged_username');
            $this->db->where('username', $log_usernames);
            $loged_details = $this->db->get('admin')->row();
            $loged_type = $loged_details->type;

            if ($loged_details->type == 'super') {

                $this->template->load('admin', 'dashboard');
            }
        } else {
            $this->session->set_userdata('logged_adminpanel', 'false');
            $this->session->set_userdata('logged_username', '');
            redirect('stablemode/index');
        }
    }

    function changepassword() {
        $_SESSION['seaval'] = '';
        $this->form_validation->set_rules('old', 'old', 'required|max_length[100]|callback_handle_password');
        $this->form_validation->set_rules('new', 'new', 'required|max_length[100]');
        $this->form_validation->set_rules('confirm', 'confirm', 'required|matches[new]|max_length[100]');
        if ($this->form_validation->run($this) == FALSE) { 
            $this->template->load('admin', 'changepassword');
        } else {
            if ($this->panel_model->update_password() == TRUE) {
                $this->session->set_flashdata('message', 'Password has been updated successfully.');
                redirect('admin/changepassword');
            } else {

                redirect('admin/changepassword');
            }
        }
    }

    function handle_password() {
        if ($this->input->post('old')) {

            $val = $this->panel_model->getpassword();
            if ($val == 0) {
                $this->form_validation->set_message('handle_password', 'This Password is Wrong !');

                return FALSE;
            } else {
                return TRUE;
            }
        }
    }
    
    
    
    function save_email() {
        $this->form_validation->set_rules('frm_email', 'From Email', 'required');
        $this->form_validation->set_rules('to_email', 'To Email', 'required');
        if ($this->form_validation->run($this) == FALSE) {
            $this->template->load('admin', 'changepassword');
        } else {

            $this->panel_model->save_email();
            $this->session->set_flashdata('message', 'E-Mail has been updated successfully.');
            redirect('admin/changepassword');
        }
    }

}
