
        <!-- END PAGE HEADER-->
        <!-- BEGIN DASHBOARD STATS 1-->

        <h1 class="page-title">Change
            <small>password</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">

            <div class="col-md-6">
                <!-- CHANGE PASSWORD TAB -->
                <form action="<?php echo base_url().'admin/changepassword' ; ?>" method="post">
                    <div class="form-group">
                        <label class="control-label">Current Password</label>
                        <input type="password" name="old"  required class="form-control" /> 
                        <span class="sa_error"><?php echo form_error('old');?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">New Password</label>
                        <input type="password"  name="new" required class="form-control" /> 
                        <span class="sa_error"><?php echo form_error('new');?></span>
                    </div>
                    <div class="form-group ">
                        <label class="control-label">Confirm New Password</label>
                        <input type="password" name="confirm"  required class="form-control" /> 
                        <span class="sa_error"><?php echo form_error('confirm');?></span>
                    </div>
                    <div class="margin-top-10">
                        <button type="submit" class="btn green"> Change Password </button>
                    </div>
                </form>
            </div>
            <div class="col-md-6">
                <!-- CHANGE PASSWORD TAB -->
                <?php $settings_row=$this->my_model->settings();?>
                <form action="<?php echo base_url().'admin/save_email' ; ?>" method="post">
                    <div class="form-group">
                        <label class="control-label">Website From Email</label>
 
                        <input type="email" name="frm_email"  required class="form-control" value="<?php echo $settings_row->from_email;?>" /> 
                        <span class="sa_error"><?php echo form_error('frm_email');?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Website To Email</label>
 
                        <input type="email" name="to_email"  required class="form-control" value="<?php echo $settings_row->to_email;?>" /> 
                        <span class="sa_error"><?php echo form_error('to_email');?></span>
                    </div>
                    <div class="margin-top-10">
                        <button type="submit" class="btn green"> Save </button>
                    </div>
                </form>
            </div>

        </div>
        <div class="clearfix"></div>
        <!-- END DASHBOARD STATS 1-->

