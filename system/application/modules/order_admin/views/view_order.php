<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> View Orders </h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
        <div class="portlet box green ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-filter"></i>Search / Sort</div>
                <div class="tools">
                    <a href="javascript:void(0);" class="collapse"></a>
                </div>
            </div>
            <div class="portlet-body form">
                <form class="form-horizontal sa_search_sorting_form" action="<?php echo base_url() . 'order_admin/view_order'; ?>" method="get"role="form">
                    <div class="form-actions">
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Search</label>
                            <div class="col-md-7">
                                <input type="text" 
                                       value="<?php
                                       if (isset($_GET['search'])) {
                                           echo $_GET['search'];
                                       }
                                       ?>"
                                       name="search"
                                       class="form-control sa_enter_submit" placeholder="Search.."> </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Custom Sort</label>
                            <div class="col-md-7">
                                <select class="form-control sa_enter_submit sa_custom_sort" name="custom_sort">
                                    <option value="">Choose</option>
                                    <?php
                                    foreach ($custom_sort_array as $key => $custom_sort) {
                                        ?>
                                        <option value="<?php echo $key; ?>" 

                                                <?php
                                                if (isset($_GET['custom_sort'])) {
                                                    if ($_GET['custom_sort'] === $key) {
                                                        echo ' selected ';
                                                    }
                                                }
                                                ?> >
                                            <?php echo ucfirst($custom_sort); ?></option>

                                    <?php } ?>
                                </select>
                                <label style="font-size: 11px;color: red;" class="sa_custom_sort_error"></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Sorting</label>
                            <div class="col-md-7">
                                <select class="form-control sa_enter_submit sa_sort" name="sort">
                                    <option value="">Choose</option>
                                    <?php
                                    foreach ($sort_array as $key => $sort) {
                                        ?>
                                        <option value="<?php echo $key; ?>" 

                                                <?php
                                                if (isset($_GET['sort'])) {
                                                    if ($_GET['sort'] === $key) {
                                                        echo ' selected ';
                                                    }
                                                }
                                                ?> >
                                            <?php echo ucfirst($sort); ?></option>

                                    <?php } ?>
                                </select>
                                <label style="font-size: 11px;color: red;" class="sa_sort_error"></label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Main Service</label>
                            <div class="col-md-7">
                                <select class="form-control sa_enter_submit" name="type">
                                    <option value="">Choose</option>
                                    <?php
                                    foreach ($media_types as $media_key => $media_type) {
                                        ?>
                                        <option value="<?php echo $media_key; ?>" 

                                                <?php
                                                if (isset($_GET['type'])) {
                                                    if ($_GET['type'] == $media_key) {
                                                        echo ' selected ';
                                                    }
                                                }
                                                ?> >
                                            <?php echo ucfirst($media_type['name']); ?></option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Service</label>
                            <div class="col-md-7">
                                <select class="form-control sa_enter_submit" name="category_id">
                                    <option value="">Choose</option>
                                    <?php
                                    foreach ($service_list as $service_key => $service) {
                                        ?>
                                        <option value="<?php echo $service->id; ?>" 

                                                <?php
                                                if (isset($_GET['category_id'])) {
                                                    if ($_GET['category_id'] == $service->id) {
                                                        echo ' selected ';
                                                    }
                                                }
                                                ?> >
                                            <?php echo ucfirst($service->category); ?></option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label class="col-md-5 control-label">Order /Work</label>
                            <div class="col-md-7">
                                <select class="form-control sa_enter_submit" name="order_status">
                                    <option value="">Choose</option>

                                    <option 
                                    <?php
                                    if (isset($_GET['order_status'])) {
                                        if ($_GET['order_status'] == "order") {
                                            echo ' selected ';
                                        }
                                    }
                                    ?> value="order">Order</option>
                                    <option 

                                        <?php
                                        if (isset($_GET['order_status'])) {
                                            if ($_GET['order_status'] == "work") {
                                                echo ' selected ';
                                            }
                                        }
                                        ?> value="work">Work</option>
                                    <option 

                                        <?php
                                        if (isset($_GET['order_status'])) {
                                            if ($_GET['order_status'] == "Finished") {
                                                echo ' selected ';
                                            }
                                        }
                                        ?> value="Finished">Finished</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 10px;">
                            <label class="col-md-5 control-label">Date Range</label>
                            <div class="col-md-7">
                                <div class="input-group input-large date-picker input-daterange" data-date="10-11-2012" data-date-format="dd-mm-yyyy">
                                    <input type="text" class="form-control sa_enter_submit" name="from_date"  readonly="" 
                                    value="<?php
                                            if (isset($_GET['from_date'])) {
                                                echo $_GET['from_date'];
                                            }
                                            ?>">
                                    <span class="input-group-addon"> to </span>
                                    <input type="text" class="form-control sa_enter_submit" name="to_date" 
                                    value="<?php
                                            if (isset($_GET['to_date'])) {
                                                echo $_GET['to_date'];
                                            }
                                            ?>"
                                           readonly=""> </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions fluid">
                        <a class="btn green" href="<?php echo base_url() . 'order_admin/view_order'; ?>">Reload</a>
                        <a class="btn green" href="<?php
                        echo base_url() . 'order_admin/order_download?' .
                        $_SERVER['QUERY_STRING'];
                        ?>">Download</a>
                        <button type="submit" class="btn green pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-image"></i>View Data</div>
            </div>
            <div class="portlet-body flip-scroll">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content">
                        <tr>
                            <th class="text-center" width="5%">ID </th>
                            <th class="text-center" width="10%">Order ID</th>
                            <th class="text-center" width="10%">Name</th>
                            <th class="text-center" width="10%">Date</th>
                            <th class="text-center" width="15%">Contact Information</th>
                            <th class="text-center" width="15%">Service Information</th>
                            <th class="text-center" width="30%">Requirement</th>
                            <th class="text-center" width="5%">Work/Order</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        if (!empty($data_list)) {
                            $i = $page_position;
                            foreach ($data_list as $key => $data) {
                                $i++;
                                $service_row = $this->my_model->RowData("ms_category", $data->category_id, "id");
                                $types = $this->my_model->media_types();
                                $main_type = $types[$data->type]['name'];
                                
                                $originalDate = $data->created_date;
                                $newDate = date("d-m-Y", strtotime($originalDate));
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i; ?></td>
                                    <td class="text-center"><?php echo $data->order_id; ?></td>
                                    <td class="text-center"><?php echo ucwords($data->user); ?></td>
                                    <td class="text-center"><?php echo $newDate; ?></td>

                                    <td class="text-center"><?php echo $data->email . ',<br>' . $data->phone; ?></td>  
                                    <td class="text-center"><?php echo ucwords($service_row->category) . ',<br>' . ucwords($main_type); ?></td>  
                                    <td class="text-center"><?php echo $data->user_message; ?></td>
                                    <td class="text-center">
                                        <select name="order_status"
                                                data-order_id="<?php echo $data->id ?>"
                                                class="sa_order_status">
                                            <option 
                                            <?php
                                            if ($data->order_status == "order") {
                                                echo " selected";
                                            }
                                            ?>
                                                value="order">Order</option>
                                            <option 
                                            <?php
                                            if ($data->order_status == "work") {
                                                echo " selected";
                                            }
                                            ?>
                                                value="work">Work</option>
                                            <option 
                                            <?php
                                            if ($data->order_status == "Finished") {
                                                echo " selected";
                                            }
                                            ?>
                                                value="Finished">Finished</option>
                                        </select>




                                    </td>




                                </tr>
                                <?php
                            }
                        }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="pagination_wrapper">
            <div class="pagination_wrapper-cover">
                <div class="pagination">  <?php echo $pagination; ?>  </div>
            </div>
        </div>

        <!-- END SAMPLE TABLE PORTLET-->
    </div>
</div>
<script>
    $("body").on("change", ".sa_order_status", function () {
        var order_status = $(this).val();
        var order_id = $(this).attr("data-order_id");

        var base_url = $(".base_url").val();
        $.ajax({
            type: 'POST',
            url: base_url + "order_admin/update_order_status",
            data: {order_status: order_status, order_id: order_id},
            success: function (response) {
                location.reload();
            }
        });
    });
</script>