<?php

class Panel_model extends CI_Model {

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
    }

    function get_order_data($table, $return_item, $perpage, $offset) {

        $this->panel_model->sort_order_data();
        if ($return_item == "count") {
            $val = $this->db->get($table);
            return $val->num_rows();
        }
        if ($return_item == "data") {
            if (!empty($perpage) && !empty($offset)) {
                $this->db->limit($perpage, $offset);
            }
            return $this->db->get($table)->result();
        }
    }

    function sort_order_data() {
        if (isset($_GET['sort'])) {
            if (isset($_GET['custom_sort'])) {
                $sort_key = $_GET['custom_sort'];
                $sort_value = $_GET['sort'];
                $this->db->order_by($sort_key, $sort_value);
            }
        } else {
            $this->db->order_by('id', 'DESC');
        }

        if (isset($_GET['search'])) {
            $search = $_GET['search'];

            $search_key = str_replace("123", "&", $search);
//            $search_key = str_replace("-", " ", $search_key);
            $search_key = strtolower($search_key);
            $like_clause_string = " order_id  LIKE '%" . $search_key . "%' ";
            $this->db->where("(" . $like_clause_string . ")", NULL, FALSE);
        }

        if (isset($_GET['type'])) {
            $media_type = $_GET['type'];

            $this->db->where("type", $media_type);
        }
        if (isset($_GET['order_status'])) {
            $order_status = $_GET['order_status'];

            $this->db->where("order_status", $order_status);
        }
        if (isset($_GET['category_id'])) {
            $category_id = $_GET['category_id'];

            $this->db->where("category_id", $category_id);
        }
        
        
           $from_date = "";
        $to_date = "";
        if (isset($_GET['from_date'])) {
            $from_date = $_GET['from_date'];
        }
        if (isset($_GET['to_date'])) {
            $to_date = $_GET['to_date'];
        }
        if ($from_date !== "") {
            $from_date = date('Y-m-d', strtotime($from_date));

            if ($to_date !== "") {
                $to_date = date('Y-m-d', strtotime($to_date));


                $from_date_1 = $from_date . ' 00:00:00';
                $to_date_1 = $to_date . ' 23:59:59';
                $this->db->where('created_date BETWEEN "' . $from_date_1 . '" AND "' . $to_date_1 . '"', NULL, FALSE);
            } else {

                $from_date_1 = $from_date . ' 00:00:00';
                $from_date_2 = $from_date . ' 23:59:59';
                $this->db->where('created_date BETWEEN "' . $from_date_1 . '" AND "' . $from_date_2 . '"', NULL, FALSE);
            }
        }
 
        
        
        
        
        
        
    }

    function update_order_status() {
        $order_status = $this->input->post('order_status');
        $order_id = $this->input->post('order_id');

        $data = array("order_status" => $order_status);
        $this->db->where('id', $order_id);
        $this->db->update('sms_orders', $data);
    }

}
