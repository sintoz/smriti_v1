<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author sms
 */
class Order_admin extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('panel_model');
        $this->load->library('ion_auth');
        $this->load->library('encryption');


        if ($this->session->userdata('logged_adminpanel') != 'true') {
            redirect('stablemode/index');
        }
    }

    function view_order() {
        $urisegments = $_SERVER['QUERY_STRING'];
        $offset = 0;

        if (isset($_GET['per_page'])) {

            $remove_segment = '&per_page=' . $_GET['per_page'];

            $urisegments = str_replace($remove_segment, '', $urisegments);
            if ($_GET['per_page'] != '') {
                $offset = $_GET['per_page'];
            } else {
                $offset = 0;
            }
        }

        $data['media_types'] = $this->my_model->media_types();
        $data['sort_array'] = $this->my_model->sort_array();
        $data['custom_sort_array'] = $this->my_model->custom_sort_array_no_order();
        $conditional_array = array("parent_id !=" => "0");
        $extra_parameter_array = array();
        $data['service_list'] = $services = $this->my_model->ResultData("ms_category", "id", "ASC", $conditional_array, $extra_parameter_array);



        $table = "sms_orders";
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'order_admin/view_order?' . $urisegments;
        $config['total_rows'] = $this->panel_model->get_order_data($table, 'count', '', '');
        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = '10';
        $config['num_links'] = 10;
        $config['uri_segment'] = 3;
        $config['prev_link'] = ' Previous';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['next_link'] = ' Next';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_list'] = $this->panel_model->get_order_data($table, 'data', $config['per_page'], $offset);
        $data['page_position'] = $offset;
        $this->template->load('admin', 'view_order', $data);
    }

    function update_order_status() {
        $this->panel_model->update_order_status();
    }

    function order_download() {
        ini_set('max_execution_time', 0);
        $table = "sms_orders";
        $order_data = $this->panel_model->get_order_data($table, 'data', '', '');
        $filename = "Order_list-" . date('d-m-Y') . '-' . date("h-i-s");

        $services = $this->my_model->media_types();


        $cms_form_excel_columns_head_list = array('Order id', 'Status', 'Email', 'Phone', 'User Message', 'Main Service', 'Service', 'Date');
        $cms_form_excel_columns_list = array('order_id', 'order_status', 'email', 'phone', 'user_message', 'type', 'category_id', 'created_date');

        $fielddata = array();
        $file_ending = "xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=$filename.xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        $sep = "\t";


        foreach ($cms_form_excel_columns_head_list as $key => $value) {
            echo $value . "\t";
        }
        print("\n");
        foreach ($order_data as $key => $order_data_row) {

            $schema_insert = "";
            foreach ($cms_form_excel_columns_list as $excel_column) {

                if ($excel_column == "type") {
                    $attr_element_field_value = $services[$order_data_row->$excel_column]['name'];
                } elseif ($excel_column == "category_id") {
                    $service_id = $order_data_row->$excel_column;
                    $service_row = $this->my_model->RowData("ms_category", $service_id, "id");
                    $attr_element_field_value = $service_row->category;
                } else if ($excel_column == "created_date") {
                    $originalDate = $order_data_row->created_date;
                    $newDate = date("d-m-Y", strtotime($originalDate));
                    $attr_element_field_value = $newDate;
                } else {
                    $attr_element_field_value = $order_data_row->$excel_column;
                }
                $schema_insert .= $attr_element_field_value . $sep;
            }
            $schema_insert = str_replace($sep . "$", "", $schema_insert);
            $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
            $schema_insert .= "\t";
            print(trim($schema_insert));
            print "\n";
        }
    }

}
