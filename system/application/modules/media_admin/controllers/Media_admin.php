<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * Description of site
 *
 * @author sms
 */
class Media_admin extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('email');
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->model('my_model');
        $this->load->model('panel_model');
        $this->load->library('ion_auth');
        $this->load->library('encryption');


        if ($this->session->userdata('logged_adminpanel') != 'true') {
            redirect('stablemode/index');
        }
    }

    public function category_management() {
        
        
        $id = "";
        $form_type = "add";

        if (!empty($_GET['type'])) {
            $mtype = $_GET['type'];
            $media_types = $this->my_model->media_types();
            $media_type_row = $media_types[$mtype];
        } else {
            redirect('admin/home');
        }
        
        
        if (!empty($_GET['id'])) {

            $data['id']=$id = $_GET['id'];
            $data['fetchDataRow']=$fetchDataRow=$this->my_model->RowData("ms_category",$id,"id");
            $data['media_type']=$media_type = $fetchDataRow->media_type;
            $data['category']=$category = $fetchDataRow->category;
            $data['parent_id']=$parent_id = $fetchDataRow->parent_id;
            $data['order_no']=$order_no = $fetchDataRow->order_no;
            $data['description']=$description = $fetchDataRow->description;
            $data['identity']=$identity = $fetchDataRow->identity;
            
            $data['extra_1']=$iextra_1 = $fetchDataRow->extra_1;
            
            $data['form_type']=$form_type = "edit";
            $data['media_type_row']=$media_type_row = $media_types[$media_type];
            $data['type_name']=$type_name=$media_type_row['name'];
            
        } else {
            $data['media_type']=$media_type = $mtype;
            $data['category']=$category = "";
            $data['parent_id']=$parent_id = "";
            $data['order_no']=$order_no = 0;
            $data['description']=$description = "";
            $data['identity']=$identity = "";
            
            $data['extra_1']=$iextra_1 ="";
                    
            $data['form_type']=$form_type = "add";
            $data['media_type_row']=$media_type_row = $media_types[$media_type];
            $data['type_name']=$type_name=$media_type_row['name'];
        }

        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category', 'Category Name', 'required');
        $this->form_validation->set_rules('identity', 'Identity', 'required|callback_handle_unique_identity');

        if ($this->form_validation->run($this) == FALSE) {

            $this->template->load('admin', 'category_management',$data);
        } else {
            
           $this->panel_model->category_management();
 
           if($form_type=="add"){
                $this->session->set_flashdata('message', "Added Successfully!..");
                redirect('media_admin/category_management?type='.$media_type); 
           }else if($form_type=="edit"){
               $this->session->set_flashdata('message', "Updated Successfully!..");
                unset($_GET['id']); 
                $redirect_url=http_build_query($_GET);
                redirect('media_admin/view_category?'.$redirect_url);   
           }
          
        }

    }

   
    function handle_unique_identity() {
        $conditional_array=array("identity"=>$this->input->post('identity'),
                                 "media_type"=>$this->input->post('mediatype'));
         $id="";
         if(!empty($this->input->post('id'))){
            $id=$this->input->post('id');
         }
        $ret = $this->my_model->unique_identity("ms_category", $conditional_array, $id);
        if ($ret) {
            $this->form_validation->set_message('handle_unique_identity', 'This Identity already exist..');
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    function handle_unique_media_identity() {
        $conditional_array=array("identity"=>$this->input->post('identity'),
                                 "media_type"=>$this->input->post('mediatype'));
         $id="";
         if(!empty($this->input->post('id'))){
            $id=$this->input->post('id');
         }
        $ret = $this->my_model->unique_identity("ms_medias", $conditional_array, $id);
        if ($ret) {
            $this->form_validation->set_message('handle_unique_media_identity', 'This Identity already exist..');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function getcategoryMediaTypeid() {
        $data['mediaTypeid']=$mediaTypeid= $this->security->xss_clean($this->input->post('media_type_id'));
        $data['parentid']=$parentid= $this->security->xss_clean($this->input->post('parentid'));
        $data['id']=$id= $this->security->xss_clean($this->input->post('id'));
        $data['type']=$type= $this->security->xss_clean($this->input->post('type'));
        
        $data['hideShowClass']=$hideShowClass = "";
        $data['parentselected']=$parentselected = "";
        if ($type == "media_data") {
            
            $data['hideShowClass']=$hideShowClass = " hide ";
        }
        if ($type == "media_category") {
            
            if ($parentid == 0) {
             $data['parentselected']=$parentselected = " selected";
            }
        }

       
       
        
        $data['categoryArray']=$categoryArray = $this->my_model->getCategoryList($mediaTypeid);
        $this->load->view("general_admin/includes/category_list",$data);
    }
    
    
    
       
    function view_category(){
        $urisegments = $_SERVER['QUERY_STRING'];
        $offset = 0;

        if (isset($_GET['per_page'])) {

            $remove_segment = '&per_page=' . $_GET['per_page'];

            $urisegments = str_replace($remove_segment, '', $urisegments);
            if ($_GET['per_page'] != '') {
                $offset = $_GET['per_page'];
            } else {
                $offset = 0;
            }
        }
        $mtype="";
        if (!empty($_GET['type'])) {
           $mtype = $_GET['type'];
            $media_types = $this->my_model->media_types();
            $media_type_row = $media_types[$mtype];
            $data['type_name']=$type_name=$media_type_row['name'];
        } 
        $data['sort_array']=$this->my_model->sort_array();
        $data['custom_sort_array']=$this->my_model->custom_sort_array();
        $data['mtype']=$mtype;
        $table='ms_category';
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'media_admin/view_category?' . $urisegments;
        $config['total_rows'] = $this->panel_model->get_media_data($table,'count','','');
        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = '10';
        $config['num_links'] = 10;
        $config['uri_segment'] = 3;
        $config['prev_link'] = ' Previous';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['next_link'] = ' Next';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_list'] = $this->panel_model->get_media_data($table,'data',$config['per_page'], $offset);
        $data['page_position'] = $offset;
        $this->template->load('admin', 'view_category', $data);  
    } 

    
    
    function removecategory(){
        $media_type=$_GET['type'];
        $id=$_GET['id'];
        
            $this->db->like('categoryidtree', '+' . $id . '+');
            $val = $this->db->get('ms_category');
            $count=$val->num_rows();
            
            $this->db->where('category',$id);
            $val = $this->db->get('ms_medias');
            $media_count=$val->num_rows();
            
             if($count==1 && $media_count==0){
                $this->my_model->DeleteData("ms_category", $id, 'id');
                $this->session->set_flashdata('message', "Removed Successfully!..");
             }else{
                $this->session->set_flashdata('message', "Cant Remove.!! This item associated with other category / item");
             }
             
       unset($_GET['id']); 
       $redirect_url=http_build_query($_GET);
       redirect('media_admin/view_category?'.$redirect_url);        
    }
    
    
    
    function media_management(){
        $id = "";
        $form_type = "add";

        if (!empty($_GET['type'])) {
            $mtype = $_GET['type'];
            $media_types = $this->my_model->media_types();
            $media_type_row = $media_types[$mtype];
        } else {
            redirect('admin/home');
        }
        
        
        if (!empty($_GET['id'])) {

            $data['id']=$id = $_GET['id'];
            $data['fetchDataRow']=$fetchDataRow=$this->my_model->RowData("ms_medias",$id,"id");
            $data['media_type']=$media_type = $fetchDataRow->media_type;
            $data['category_id']=$category_id = $fetchDataRow->category;
            $data['title']=$title = $fetchDataRow->title;
            $data['identity']=$identity = $fetchDataRow->identity;
            $data['short_title']=$short_title = $fetchDataRow->short_title;
            $data['short_description']=$short_description = $fetchDataRow->short_description;
            $data['description']=$description = $fetchDataRow->description;
            $data['media_status']=$media_status = $fetchDataRow->media_status;
            $data['order_no']=$order_no = $fetchDataRow->order_no;
            $data['form_type']=$form_type = "edit";
            $data['media_type_row']=$media_type_row = $media_types[$media_type];
            $data['type_name']=$type_name=$media_type_row['name'];
            
        } else {
            
            $data['media_type']=$media_type = $mtype;
            $data['category_id']=$category_id ="";
            $data['title']=$title = "";
            $data['identity']=$identity = "";
            $data['short_title']=$short_title = "";
            $data['short_description']=$short_description = "";
            $data['description']=$description ="";
            $data['media_status']=$media_status = "";
            $data['order_no']=$order_no = 0;
            $data['form_type']=$form_type = "add";
            $data['media_type_row']=$media_type_row = $media_types[$media_type];
            $data['type_name']=$type_name=$media_type_row['name'];
        }

        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('identity', 'Identity', 'required|callback_handle_unique_media_identity');
 
        if ($this->form_validation->run($this) == FALSE) {

            $this->template->load('admin', 'media_management',$data);
            
        } else {
            
           $this->panel_model->media_management();
           
           
           if($form_type=="add"){
               $this->session->set_flashdata('message', "Added Successfully!..");
                redirect('media_admin/media_management?type='.$media_type); 
           }else if($form_type=="edit"){
               $this->session->set_flashdata('message', "Updated Successfully!..");
                unset($_GET['id']); 
                $redirect_url=http_build_query($_GET);
                redirect('media_admin/view_media?'.$redirect_url);   
           }
          
        } 
    }

    
     function view_media(){
        $urisegments = $_SERVER['QUERY_STRING'];
        $offset = 0;

        if (isset($_GET['per_page'])) {

            $remove_segment = '&per_page=' . $_GET['per_page'];

            $urisegments = str_replace($remove_segment, '', $urisegments);
            if ($_GET['per_page'] != '') {
                $offset = $_GET['per_page'];
            } else {
                $offset = 0;
            }
        }
        $mtype="";
        if (!empty($_GET['type'])) {
           $mtype = $_GET['type'];
            $media_types = $this->my_model->media_types();
            $media_type_row = $media_types[$mtype];
            $data['type_name']=$type_name=$media_type_row['name'];
        } 
        $data['sort_array']=$this->my_model->sort_array();
        $data['custom_sort_array']=$this->my_model->custom_sort_array();
        $data['item_status_array']=$this->my_model->item_status_array();
        $data['mtype']=$mtype;
        $table="ms_medias";
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'media_admin/view_media?' . $urisegments;
        $config['total_rows'] = $this->panel_model->get_media_data($table,'count','','');
        $config['enable_query_strings'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['per_page'] = '10';
        $config['num_links'] = 10;
        $config['uri_segment'] = 3;
        $config['prev_link'] = ' Previous';
        $config['prev_tag_open'] = '';
        $config['prev_tag_close'] = '';
        $config['next_link'] = ' Next';
        $config['next_tag_open'] = '';
        $config['next_tag_close'] = '';
        $this->pagination->initialize($config);
        $data['pagination'] = $this->pagination->create_links();
        $data['data_list'] = $this->panel_model->get_media_data($table,'data',$config['per_page'], $offset);
        $data['page_position'] = $offset;
        $this->template->load('admin', 'view_media', $data);  
    } 
    
    
     function removemedia(){
        $media_type=$_GET['type'];
        $id=$_GET['id'];

        $this->my_model->DeleteData("ms_medias", $id, 'id');
        $this->session->set_flashdata('message', "Removed Successfully!..");

             
       unset($_GET['id']); 
       $redirect_url=http_build_query($_GET);
       redirect('media_admin/view_media?'.$redirect_url);        
    }
     
    
    
}

