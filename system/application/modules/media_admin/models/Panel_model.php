<?php
class Panel_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Kolkata');
		
    }

    function category_management() {

        $mediatype = $this->security->xss_clean($this->input->post('mediatype'));
        $category_parent = $this->security->xss_clean($this->input->post('category_parent'));
        $category = $this->security->xss_clean($this->input->post('category'));
        $identity = $this->security->xss_clean($this->input->post('identity'));
        $description = $this->security->xss_clean($this->input->post('description'));
        $order = $this->security->xss_clean($this->input->post('order'));
        $id = $this->security->xss_clean($this->input->post('id'));
        $form_type = $this->security->xss_clean($this->input->post('form_type'));
        $extra_1 = $this->security->xss_clean($this->input->post('extra_1'));
      
        $upload_image_1="";
        if(!empty($_POST["upload_image_1"]) && $form_type=="add"){
           $upload_image_1= $this->security->xss_clean($this->input->post('upload_image_1')); 
        }

        $data = array(
            'media_type' => $mediatype,
            'parent_id' => $category_parent,
            'category' => $category,
            'identity' => $identity,
            'description' => $description,
            'category_file' => $upload_image_1,
            'extra_1' => $extra_1,
            'order_no' => $order
        );
        
        if (empty($id)) {
            $this->db->insert('ms_category', $data);
            $insert_id = $this->db->insert_id();
        } else {
            $cat_details = $this->my_model->RowData('ms_category', $id, 'id');
            
            if(!empty($_POST["upload_image_1"]) && $form_type=="edit"){
                $new_files= $this->security->xss_clean($this->input->post('upload_image_1'));
                
                $category_file=$cat_details->category_file;
                $exist_files= json_decode($cat_details->category_file,TRUE);
                if(empty($category_file)){
                 $exist_files=[];  
                }
                $new_files= json_decode($new_files,TRUE);
                $upload_image_1=array_merge($exist_files,$new_files);
                $upload_image_1=json_encode($upload_image_1);
            }else{
                 $upload_image_1=$cat_details->category_file;
            }
            $data['category_file']=$upload_image_1;
            
            $this->db->where('id', $id);
            $this->db->update('ms_category', $data);
            $insert_id=$id;
        }

   
        $get_val = $this->my_model->tree_creation($insert_id, $insert_id, 'media_category');
        
           if ($category_parent != 0) {


            $parent_row = $this->my_model->RowData('ms_category', $category_parent, 'id');
            $parent_category_slug = $parent_row->identity;
            $get_val = $this->my_model->tree_creation($category_parent, $insert_id, 'media_category');



            $data2 = array(
                'categoryidtree' => $get_val['category_ids'],
                'categorynametree' => $get_val['category_names'],
                'categoryidentitytree' => $get_val['category_slugs'],
                'categoryfull' => $get_val['category_full'],
                'parent_main_identity' => $get_val['cat_parent_route'],
                'parent_main_id' => $get_val['cat_parent_id'],
            );
        } else {

            $data2 = array(
                'categoryidtree' => $get_val['category_ids'],
                'categorynametree' => $get_val['category_names'],
                'categoryidentitytree' => $get_val['category_slugs'],
                'categoryfull' => $get_val['category_full'],
                'parent_main_identity' => $get_val['cat_parent_route'],
                'parent_main_id' => $get_val['cat_parent_id'],
            );
        }
        
        $this->db->where('id', $insert_id);
        $this->db->update('ms_category', $data2);
        
        
        if ($form_type == "edit") {
            
            $category_name2 = $this->input->post('category');
            $category_name2 = strtolower($category_name2);
            $category_identity2 = $this->input->post('identity');
            $category_identity2 = strtolower($category_identity2);
            
            
            $query2 = "UPDATE ms_category SET "
                    . "categorynametree = REPLACE(categorynametree, '+" . strtolower($cat_details->category) . "+', '+" . $category_name2 . "+'), "
                    . "categoryidentitytree = REPLACE(categoryidentitytree, '+" . strtolower($cat_details->identity) . "+', '+" . $category_identity2 . "+'), "
                    . "categoryfull = REPLACE(categoryfull, '__" . strtolower($cat_details->category) . "__" . strtolower($cat_details->identity) . "+', '__" . $category_name2 . "__" . $category_identity2 . "+') "
                    . "WHERE parent_main_id='" . $cat_details->parent_main_id . "'";

            $this->db->query($query2);


            $query = "UPDATE ms_medias SET "
                    . "categorynametree = REPLACE(categorynametree, '+" . strtolower($cat_details->category) . "+', '+" . $category_name2 . "+'), "
                    . "categoryidentitytree = REPLACE(categoryidentitytree, '+" . strtolower($cat_details->identity) . "+', '+" . $category_identity2 . "+'), "
                    . "categoryfull = REPLACE(categoryfull, '__" . strtolower($cat_details->category) . "__" . strtolower($cat_details->identity) . "+', '__" . $category_name2 . "__" . $category_identity2 . "+') "
                    . "WHERE parent_main_id='" . $cat_details->parent_main_id . "'";
            
            $this->db->query($query);
        }
    }
    
    
    function media_management() {

        $mediatype = $this->security->xss_clean($this->input->post('mediatype'));
        $category = $this->security->xss_clean($this->input->post('category'));
        $identity = $this->security->xss_clean($this->input->post('identity'));
        $title = $this->security->xss_clean($this->input->post('title'));
        $short_title = $this->security->xss_clean($this->input->post('short_title'));
        $short_description = $this->security->xss_clean($this->input->post('short_description'));
        $description = $this->security->xss_clean($this->input->post('description'));
        $order = $this->security->xss_clean($this->input->post('order'));
        $media_status = $this->security->xss_clean($this->input->post('media_status'));
        $id = $this->security->xss_clean($this->input->post('id'));
        $form_type = $this->security->xss_clean($this->input->post('form_type'));
      
        $upload_image_1="";
        if(!empty($_POST["upload_image_1"]) && $form_type=="add"){
           $upload_image_1= $this->security->xss_clean($this->input->post('upload_image_1')); 
        }

        $data = array(
            'media_type' => $mediatype,
            'category' => $category,
            'identity' => $identity,
            'title' => $title,
            'short_title' => $short_title,
            'short_description' => $short_description,
            'description' => $description,
            'media_status' => $media_status,
            'media_file' => $upload_image_1,
            'order_no' => $order
        );
        
        if (empty($id)) {
            $this->db->insert('ms_medias', $data);
            $insert_id = $this->db->insert_id();
        } else {
            $media_details = $this->my_model->RowData('ms_medias', $id, 'id');
            
            if(!empty($_POST["upload_image_1"]) && $form_type=="edit"){
                $new_files= $this->security->xss_clean($this->input->post('upload_image_1'));
                
                $media_file=$media_details->media_file;
                $exist_files= json_decode($media_details->media_file,TRUE);
                if(empty($media_file)){
                 $exist_files=[];  
                }
                $new_files= json_decode($new_files,TRUE);
                $upload_image_1=array_merge($exist_files,$new_files);
                $upload_image_1=json_encode($upload_image_1);
            }else{
                 $upload_image_1=$media_details->media_file;
            }
            $data['media_file']=$upload_image_1;
            
            $this->db->where('id', $id);
            $this->db->update('ms_medias', $data);
            $insert_id=$id;
        }
        
        $category_details = $this->my_model->RowData('ms_category', $category, 'id');

           $data2 = array(
                'categoryidtree' => $category_details->categoryidtree,
                'categorynametree' => $category_details->categorynametree,
                'categoryidentitytree' => $category_details->categoryidentitytree,
                'categoryfull' =>$category_details->categoryfull,
                'parent_main_identity' => $category_details->parent_main_identity,
                'parent_main_id' => $category_details->parent_main_id,
            );
     
        
        $this->db->where('id', $insert_id);
        $this->db->update('ms_medias', $data2);
        
        

    }
    
    
    function get_media_data($table,$return_item,$perpage,$offset){
            $type=$_GET['type'];
            if($table=="ms_category"){
              $this->panel_model->sort_category_data();  
            }else if($table=="ms_medias"){
               $this->panel_model->sort_media_data();   
            }
            
            if ($return_item == "count") {
                       $this->db->where('media_type', $type);
                $val = $this->db->get($table);
                return $val->num_rows();
            }
            if ($return_item == "data") {
                        $this->db->where('media_type', $type);
                        $this->db->limit($perpage, $offset);
                 return $this->db->get($table)->result();
            }
    }
    
   function sort_category_data(){
      if (isset($_GET['sort'])) {
            if (isset($_GET['custom_sort'])) {
                $sort_key = $_GET['custom_sort'];
                $sort_value = $_GET['sort'];
                $this->db->order_by($sort_key, $sort_value);
            }
        } else {
            $this->db->order_by('id', 'DESC');
        }
        
      if (isset($_GET['search'])) {
            $search = $_GET['search'];

            $search_key = str_replace("123", "&", $search);
            $search_key = str_replace("-", " ", $search_key);
            $search_key = strtolower($search_key);
            $like_clause_string = " category  LIKE '%" . $search_key . "%' ";
            $this->db->where("(" . $like_clause_string . ")", NULL, FALSE);
        }     
        
   } 
   function sort_media_data(){
      if (isset($_GET['sort'])) {
            if (isset($_GET['custom_sort'])) {
                $sort_key = $_GET['custom_sort'];
                $sort_value = $_GET['sort'];
                $this->db->order_by($sort_key, $sort_value);
            }
        } else {
            $this->db->order_by('id', 'DESC');
        }
        
       if (isset($_GET['search'])) {
            $search = $_GET['search'];

            $search_key = str_replace("123", "&", $search);
            $search_key = str_replace("-", " ", $search_key);
            $search_key = strtolower($search_key);
            $like_clause_string = " title  LIKE '%" . $search_key . "%' ";
            $this->db->where("(" . $like_clause_string . ")", NULL, FALSE);
        }   
        
       if (isset($_GET['status'])) {
            $media_status = $_GET['status'];

            $this->db->where("media_status",$media_status);
        }     
        
   } 

}