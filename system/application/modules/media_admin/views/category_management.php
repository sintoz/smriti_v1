
<!-- END PAGE HEADER-->
<!-- BEGIN DASHBOARD STATS 1-->
<h1 class="page-title">
    <?php
    echo strtoupper($form_type).' '.ucwords($type_name);
    ?><small> Category</small>
</h1>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="row m-heading-1 border-green m-bordered">
    <div class="col-md-8 col-md-offset-2">
        <!-- CHANGE PASSWORD TAB -->
        <form action="<?php echo base_url().'media_admin/category_management?'. $_SERVER['QUERY_STRING'];?>" method="post" enctype='multipart/form-data' class="sa_multiple_upload_form" id="sa_multiple_upload_form">
            <input type="hidden" name="id" value="<?php if(!empty($id)){echo $id;}?>">
            <input type="hidden" class="sa_mediatype" name="mediatype" value="<?php echo $media_type; ?>">
            <input type="hidden"  name="form_type" value="<?php echo $form_type; ?>">
            <div class="form-group">
                <label class="control-label">Select Parent Category</label>
                <select class="form-control sa_category_parent" 
                        name="category_parent" required="">
                    <option value="">Select...</option>
                </select>
                <span class="sa_error"></span>
            </div>
            <div class="form-group">
                <label class="control-label">Category Name</label>
                <input type="text" name="category"
                       value="<?php echo $category; ?>"
                       required class="form-control sa_identity_ref" /> 
                <span class="sa_error"><?php echo form_error('category'); ?></span>
            </div>
            <div class="form-group">
                <label class="control-label">Identification Name</label>
                <input type="text" name="identity"
                       value="<?php echo $identity; ?>"
                       required class="form-control sa_identity_val" /> 
                <span class="sa_error"><?php echo form_error('identity'); ?></span>
            </div>
            
            
            <?php 
            $fileuploadnames=$this->my_model->fileuploadnames();
            $attachment_data['label']="Category Image"; // file label
            $attachment_data['name']="upload_image"; // file input name
            $attachment_data['post_name']="upload_image_1"; // attachment save field [post data]
            $attachment_data['count']="multiple";  // multiple upload or single upload
            $attachment_data['attachment_count']=$fileuploadnames["media_category_image"]; // form attachment count
            $attachment_data['image_type']="media_category_image"; // image type
            $attachment_data['form_type']=$form_type; // form type
            $attachment_data['upload']=$this->my_model->upload_rules();// upload rules array

            $this->load->view('attachment_view/attachment',$attachment_data);
            ?>
            
            
           
            <div class="form-group">
                 <label class="control-label">Category Description</label>
                 <textarea name="description" 
                           class="form-control ckeditor" ><?php echo $description; ?></textarea>
                 <span class="sa_error"></span>
             </div>
            <div class="form-group ">
                <label class="control-label">Price</label>
                <input type="text" name="extra_1" 
                       value="<?php echo $extra_1; ?>"
                       class="form-control" /> 
                <span class="sa_error"></span>
            </div>
            <div class="form-group ">
                <label class="control-label">Order</label>
                <input type="number" min="0" name="order" 
                       value="<?php echo $order_no; ?>"
                       class="form-control" /> 
                <span class="sa_error"></span>
            </div>
            <div class="margin-top-10">
                <button type="button" class="btn green pull-right sa_attach_submit "> Save </button>
            </div>
        </form>
    </div>

</div>
<div class="clearfix"></div>
<!-- END DASHBOARD STATS 1-->
<script type="text/javascript">
    $(document).ready(function () {
        var media_type_id = $(".sa_mediatype").val();
        getcategoryMediaTypeid(media_type_id);
    });

    function getcategoryMediaTypeid(media_type_id) {
        var parentid = "<?php echo $parent_id ?>";
        var id = "<?php if(!empty($id)){echo $id;}?>";
        var type = "media_category";
        var base_url=$(".base_url").val();
        $.ajax({
            url: base_url+"media_admin/getcategoryMediaTypeid",
            data: {media_type_id: media_type_id, parentid: parentid, id: id, type: type},
            type: "POST",
            success: function (response)
            {

                $(".sa_category_parent").html(response);

            }
        });

    }


</script>