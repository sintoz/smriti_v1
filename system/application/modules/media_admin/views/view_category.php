        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> <?php echo ucwords(str_replace("_"," ",$type_name));?> </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                         <div class="row">
                             <div class="col-md-12">
                                <div class="portlet box green ">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-filter"></i>Search / Sort</div>
                                        <div class="tools">
                                            <a href="javascript:void(0);" class="collapse"></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body form">
                                        <form class="form-horizontal sa_search_sorting_form" action="<?php echo base_url() . 'media_admin/view_category?type='.$mtype; ?>" method="get"role="form">
                                            <div class="form-actions">
                                                <div class="col-md-4">
                                                    <label class="col-md-5 control-label">Search</label>
                                                    <div class="col-md-7">
                                                        <input type="text" 
                                                               value="<?php
                                                                if (isset($_GET['search'])) {
                                                                    echo $_GET['search'];
                                                                }
                                                                ?>"
                                                                name="search"
                                                               class="form-control sa_enter_submit" placeholder="Search.."> </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="col-md-5 control-label">Custom Sort</label>
                                                    <div class="col-md-7">
                                                        <select class="form-control sa_enter_submit sa_custom_sort" name="custom_sort">
                                                            <option value="">Choose</option>
                                                            <?php
                                                foreach ($custom_sort_array as $key => $custom_sort) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>" 

                                                            <?php
                                                            if (isset($_GET['custom_sort'])) {
                                                                if ($_GET['custom_sort'] === $key) {
                                                                    echo ' selected ';
                                                                }
                                                            }
                                                            ?> >
                                                        <?php echo ucfirst($custom_sort); ?></option>

                                                <?php } ?>
                                                        </select>
                                                        <label style="font-size: 11px;color: red;" class="sa_custom_sort_error"></label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <label class="col-md-5 control-label">Sorting</label>
                                                    <div class="col-md-7">
                                                        <select class="form-control sa_enter_submit sa_sort" name="sort">
                                                            <option value="">Choose</option>
                                                            <?php
                                                foreach ($sort_array as $key => $sort) {
                                                    ?>
                                                    <option value="<?php echo $key; ?>" 

                                                            <?php
                                                            if (isset($_GET['sort'])) {
                                                                if ($_GET['sort'] === $key) {
                                                                    echo ' selected ';
                                                                }
                                                            }
                                                            ?> >
                                                        <?php echo ucfirst($sort); ?></option>

                                                <?php } ?>
                                                        </select>
                                                        <label style="font-size: 11px;color: red;" class="sa_sort_error"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-actions fluid">
                                                <a class="btn green" href="<?php echo base_url().'media_admin/view_category?type='.$mtype;?>">Reload</a>
                                                <button type="submit" class="btn green pull-right">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            </div>
                        
                        
                        
                        
                        
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-image"></i>View Category</div>
                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead class="flip-content">
                                                <tr>
                                                    <th class="text-center" width="10%">ID </th>
                                                    <th class="text-center" width="20%">Category</th>
                                                    <th class="text-center">Parent Category</th>
                                                    <th class="text-center" width="20%">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               
                                                    <?php
                                                if (!empty($data_list)) {
                                                    $i = $page_position;
                                                    foreach ($data_list as $key => $data) {
                                                          $i++;
                                                        
                                                        if($data->parent_id==0){
                                                           $parent="<span class='label label-sm label-info'>Parent</span>"; 
                                                        }else{
                                                            $parent_row=$this->my_model->RowData("ms_category", $data->parent_id, "id");
                                                            $parent=$parent_row->category;
                                                            
                                                        }
                                                        ?>
                                                        <tr>
                                                            <td class="text-center"><?php echo $i; ?></td>
                                                            <td class="text-center"><?php echo $data->category;?></td>
                                                            <td class="text-center"><?php echo $parent; ?></td>  
                                                            <td>
                                                                <div class="btn-group">
                                                                    <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions<i class="fa fa-angle-down"></i></button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                <li>
                                                                    <a href="<?php echo base_url().'media_admin/category_management?'.$_SERVER['QUERY_STRING'].'&id='.$data->id; ?>">
                                                                        <i class="fa fa-edit"></i>Edit</a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:void(0);"
                                                                      onclick="removeitem('<?php echo base_url().'media_admin/removecategory?'.$_SERVER['QUERY_STRING'].'&id='.$data->id;?>')">
                                                                        <i class="fa fa-trash-o" ></i> Delete </a>
                                                                </li>
                                                            </ul>
                                                                </div>
                                                            </td>      
                                                                    
                                                                    
                                                                    
                                                        </tr>
                                                    <?php }
                                                }
                                                ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="pagination_wrapper">
                                    <div class="pagination_wrapper-cover">
                                        <div class="pagination">  <?php echo $pagination; ?>  </div>
                                    </div>
                                </div>
                               
                                <!-- END SAMPLE TABLE PORTLET-->
                            </div>
                        </div>
