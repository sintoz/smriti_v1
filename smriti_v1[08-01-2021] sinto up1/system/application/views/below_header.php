<header>
    <div class="main-header">
        <div class="container">
            <nav class="navbar navbar-expand-lg hdr-nav navbar-light">

                <div class="brand-wrapper">
                    <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/logo-smritidesign.svg" alt="Smriti Design"/></a>
                </div>

                <div class="top-line">
                    <div class="top-right">
                        <div class="top-contact">T: (+91) 487 242 1229</div>
                        <div class="top-contact">M: (+91) 944 640 1229</div>
                        <div class="top-contact">E: <a href="mailto:smritidesign@yahoo.com">smritidesign@yahoo.com</a></div>
                    </div>
                </div><!--top-line-->
                <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menu" aria-expanded="false" aria-label="Toggle navigation"><span class="close"></span><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse hover-dropdown flex-column" id="menu">
                    <ul class="navbar-nav bg ml-auto">
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>about">About Us</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services <i class="fa fa-angle-down m-l-5"></i>
                            </a>
                            <ul class="b-none dropdown-menu animated fadeIn">
                                <?php
                                if (!empty($services)) {
                                    $i = 1;
                                    foreach ($services as $serkey => $service) {
                                        ?>
                                        <li><a class="dropdown-item" href="<?php echo base_url() . 'services?service_id=' . $serkey; ?>"><?php echo ucwords($service['name']); ?></a></li>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </ul>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>portfolio">Portfolio</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>careers">Careers</a></li>
                        <li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>contact">Contact Us</a></li>
                    </ul>

                    <div class="mob-box">
                        <div class="top-contact">T: &nbsp;(+91) 487 242 1229</div>
                        <div class="top-contact">M: &nbsp;(+91) 944 640 1229</div>
                        <div class="top-contact">E: &nbsp;<a href="mailto:smritidavid@gmail.com">smritidavid@gmail.com</a></div>
                    </div><!--mob-box-->

                </div>
            </nav>
        </div><!--container-->
    </div><!--main-header-->
</header>