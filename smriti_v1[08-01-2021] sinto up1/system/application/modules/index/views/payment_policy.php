<section class="sub-banner-sec wow fadeIn">
    <div class="container">
        <h2>Payment Policy</h2>
    </div>
</section>

<section class="breadcrumb-sec wow fadeIn">
    <div class="container">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
            <li class="breadcrumb-item active">Payment Policy</li>
        </ol>
    </div>	
</section>

<section class="general-sec">
    <div class="container">

        <h5>Payment</h5>
        <p>The customer will be provided with an Approval Form or Proof Email, and an Invoice prior to final publication. At this time the remainder of the amount due will become payable and the customer will also be required to sign and return the Approval Form or signify approval by email to Smriti Design.</p>

        <p>Any invoice queries must be submitted by email within 14 days of the invoice date.</p>

        <p>Accounts which remain outstanding for 30 days after the date of invoice, will incur late payment interest charge at the Bank of India Base Rate plus 8% on the outstanding amount from the date due until the date of payment.</p>

        <p>Payments may be made by online transfer, UPI Payment.</p>

        <p>Payments made by cheque must be previously agreed and may be subject to an administration charge. Cheques should not be sent in regular mail unless sent recorded delivery.</p>

        <p>Publication and/or release of work done by Smriti Design on behalf of the client, may not take place before cleared funds have been received.</p>

        <p>Returned cheques will incur an additional fee of Rs.500 per returned cheque. Smriti Design reserves the right to consider an account to be in default in the event of a returned cheque.</p>


        <h5 class="text-center">Bank Details</h5>

        <div class="box">
            <div class="txt-b">
                Name: Smriti Design<br>
                A/c No: 12800200009883<br>
                IFSC: FDRL0001280<br>
                Federal Bank, ST Nagar Branch, Thrissur
            </div>
        </div>


        <div class="box">
            <h4>UPI Payments accepted here</h4>
            <h3>Smriti Design</h3>
            <div class="txt-one">Scan QR Code below with your PSP App</div>
            <div class="qr-image"><img src="<?php echo base_url() . 'assets/web_end/' ?>images/qr-code-payment.svg" alt=""/></div>	
            <div class="txt-two">Or Pay to VPA</div>
            <div class="txt-three">smritidavid@okaxis</div>
        </div>

    </div>
</section>