(function () {
    'use strict';


    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('header').addClass("sticky");
        } else {
            $('header').removeClass("sticky");
        }
    });


    var base_url = $(".base_url").val();

    /* owl */

    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            loop: true,
            margin: 25,
            nav: true,
            dots: false,
            smartSpeed: 500,
            autoplay: true,
            slideBy: 5,
            navText: ["<img src='" + base_url + "assets/web_end/images/arrow-pre.png'>", "<img src='" + base_url + "assets/web_end/images/arrow-next.png'>"],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                    margin: 10,
                },
                480: {
                    items: 3,
                    margin: 10,
                },
                576: {
                    items: 3,
                    margin: 10,
                },

                768: {
                    items: 4,
                    margin: 20,
                },

                992: {
                    items: 5,
                },

                1200: {
                    items: 5,
                }
            }
        });
    });


    $(document).ready(function () {
        $("#owl-process").owlCarousel({
            loop: true,
            margin: 25,
            nav: false,
            dots: true,
            smartSpeed: 500,
            autoplay: false,
            slideBy: 1,
            navText: ["<img src='" + base_url + "assets/web_end/images/arrow-pre.png'>", "<img src='" + base_url + "assets/web_end/images/arrow-next.png'>"],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                },

                576: {
                    items: 1,
                },

                768: {
                    items: 3,
                },

                992: {
                    items: 3,
                },

                1200: {
                    items: 3,
                }
            }
        });
    });


    $(document).ready(function () {
        $("#owl-clients").owlCarousel({
            loop: true,
            margin: 10,
            nav: false,
            dots: true,
            smartSpeed: 500,
            autoplay: true,
            slideBy: 8,
            navText: ["<img src='" + base_url + "assets/web_end/images/arrow-pre.png'>", "<img src='" + base_url + "assets/web_end/images/arrow-next.png'>"],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                },
                576: {
                    items: 3,
                },

                768: {
                    items: 4,
                },

                992: {
                    items: 6,
                },

                1200: {
                    items: 6,
                },

                1600: {
                    items: 8,
                }

            }
        });
    });



    $(document).ready(function () {
        $("#owl-testimonials").owlCarousel({
            loop: true,
            margin: 30,
            nav: true,
            dots: false,
            smartSpeed: 1000,
            autoplay: true,
            autoplayHoverPause: true,
            slideBy: 1,
            navText: ["<img src='" + base_url + "assets/web_end/images/arrow-pre.png'>", "<img src='" + base_url + "assets/web_end/images/arrow-next.png'>"],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                },
                576: {
                    items: 1,
                },

                768: {
                    items: 1,
                },

                992: {
                    items: 1,
                },

                1200: {
                    items: 1,
                }
            }
        });
    });



    $(document).ready(function () {
        $("#owl-similat-items").owlCarousel({
            loop: true,
            margin: 30,
            nav: false,
            dots: true,
            smartSpeed: 500,
            autoplay: true,
            slideBy: 4,
            navText: ["<img src='" + base_url + "assets/web_end/images/arrow-pre.png'>", "<img src='" + base_url + "assets/web_end/images/arrow-next.png'>"],
            responsiveClass: true,
            responsive: {
                0: {
                    items: 2,
                },
                576: {
                    items: 2,
                },

                768: {
                    items: 3,
                },

                992: {
                    items: 4,
                },

                1200: {
                    items: 4,
                }
            }
        });
    });





    $('.modal').on('hidden.bs.modal', function (e) {
        $(this).find('iframe').attr('src', $(this).find('iframe').attr('src'));
    });






}());





(function () {
    // Back to Top - by CodyHouse.co
    var backTop = document.getElementsByClassName('js-cd-top')[0],
            // browser window scroll (in pixels) after which the "back to top" link is shown
            offset = 300,
            //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
            offsetOpacity = 1200,
            scrollDuration = 700,
            scrolling = false;
    if (backTop) {
        //update back to top visibility on scrolling
        window.addEventListener("scroll", function (event) {
            if (!scrolling) {
                scrolling = true;
                (!window.requestAnimationFrame) ? setTimeout(checkBackToTop, 250) : window.requestAnimationFrame(checkBackToTop);
            }
        });
        //smooth scroll to top
        backTop.addEventListener('click', function (event) {
            event.preventDefault();
            (!window.requestAnimationFrame) ? window.scrollTo(0, 0) : scrollTop(scrollDuration);
        });
    }

    function checkBackToTop() {
        var windowTop = window.scrollY || document.documentElement.scrollTop;
        (windowTop > offset) ? addClass(backTop, 'cd-top--show') : removeClass(backTop, 'cd-top--show', 'cd-top--fade-out');
        (windowTop > offsetOpacity) && addClass(backTop, 'cd-top--fade-out');
        scrolling = false;
    }

    function scrollTop(duration) {
        var start = window.scrollY || document.documentElement.scrollTop,
                currentTime = null;

        var animateScroll = function (timestamp) {
            if (!currentTime)
                currentTime = timestamp;
            var progress = timestamp - currentTime;
            var val = Math.max(Math.easeInOutQuad(progress, start, -start, duration), 0);
            window.scrollTo(0, val);
            if (progress < duration) {
                window.requestAnimationFrame(animateScroll);
            }
        };

        window.requestAnimationFrame(animateScroll);
    }

    Math.easeInOutQuad = function (t, b, c, d) {
        t /= d / 2;
        if (t < 1)
            return c / 2 * t * t + b;
        t--;
        return -c / 2 * (t * (t - 2) - 1) + b;
    };

    //class manipulations - needed if classList is not supported
    function hasClass(el, className) {
        if (el.classList)
            return el.classList.contains(className);
        else
            return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'));
    }
    function addClass(el, className) {
        var classList = className.split(' ');
        if (el.classList)
            el.classList.add(classList[0]);
        else if (!hasClass(el, classList[0]))
            el.className += " " + classList[0];
        if (classList.length > 1)
            addClass(el, classList.slice(1).join(' '));
    }
    function removeClass(el, className) {
        var classList = className.split(' ');
        if (el.classList)
            el.classList.remove(classList[0]);
        else if (hasClass(el, classList[0])) {
            var reg = new RegExp('(\\s|^)' + classList[0] + '(\\s|$)');
            el.className = el.className.replace(reg, ' ');
        }
        if (classList.length > 1)
            removeClass(el, classList.slice(1).join(' '));
    }
})();

$(function () {
    $('#example').horizontalTimeline();
});

